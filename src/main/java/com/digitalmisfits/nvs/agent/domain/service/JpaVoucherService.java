package com.digitalmisfits.nvs.agent.domain.service;

import com.digitalmisfits.nvs.agent.domain.entity.AirtimeVoucher;
import com.digitalmisfits.nvs.agent.domain.entity.NetworkOperator;
import com.digitalmisfits.nvs.agent.domain.repository.AirtimeVoucherRepository;
import com.digitalmisfits.nvs.agent.domain.repository.NetworkOperatorRepository;
import com.digitalmisfits.nvs.agent.service.VoucherService;
import com.digitalmisfits.nvs.agent.telephony.ChannelOperator;
import com.digitalmisfits.nvs.agent.telephony.Voucher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JpaVoucherService implements VoucherService {

    @Autowired
    private NetworkOperatorRepository operatorRepository;

    @Autowired
    private AirtimeVoucherRepository voucherRepository;

    @Override
    public Voucher findVoucher(ChannelOperator channelOperator) throws VoucherNotAvailableException {

        NetworkOperator networkOperator = operatorRepository.findGeographicalOperator(
                channelOperator.getIso3166CountryCode(), channelOperator.getNetworkName());

        List<AirtimeVoucher> vouchers = voucherRepository.findAvailableAirtimeVouchers(networkOperator, new PageRequest(0, 1));
        
        if(vouchers.isEmpty()) {
            throw new VoucherNotAvailableException();
        }
        
        return new Voucher(vouchers.get(0).getNumber());
    }
}
