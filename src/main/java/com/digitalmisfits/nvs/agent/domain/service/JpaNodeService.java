package com.digitalmisfits.nvs.agent.domain.service;

import com.digitalmisfits.nvs.agent.domain.repository.NodeRepository;
import com.digitalmisfits.nvs.agent.net.Node;
import com.digitalmisfits.nvs.agent.service.NodeService;
import com.google.common.collect.ImmutableSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;

@Service
public class JpaNodeService implements NodeService {

    @Autowired
    NodeRepository nodeRepository;

    @Override
    public Collection<Node> findAllRegistered() {

        final ImmutableSet.Builder<Node> nodes = new ImmutableSet.Builder<>();

        nodeRepository.findAll().forEach((node) -> {
            try {
                nodes.add(new Node(node.getId(), node.getIso3166Code(), InetAddress.getByName(node.getRemoteHost())));
            } catch (UnknownHostException e) {
                throw new RuntimeException(String.format("Host %s is not a valid internet address", node.getRemoteHost(), e));
            }
        });

        return nodes.build();
    }
}
