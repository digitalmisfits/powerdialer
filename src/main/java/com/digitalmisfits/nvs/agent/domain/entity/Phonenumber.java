package com.digitalmisfits.nvs.agent.domain.entity;

import com.digitalmisfits.nvs.agent.telephony.Channel;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "phonenumbers")
public class Phonenumber {

    @Id
    @GeneratedValue
    private Long id;

    /**
     * The origination network operator
     */
    @OneToOne
    @JoinColumn(name = "operator_id", nullable = false)
    public NetworkOperator networkOperator;

    /**
     * The termination network destination
     */
    @OneToOne
    @JoinColumn(name = "destination_id", nullable = false)
    public NetworkDestinationCode networkDestination;

    /**
     * International subscriber
     */
    @Column(nullable = false)
    private String subscriber;

    /**
     * Connection state
     */
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Channel.ConnectionState connectionState;

    /**
     * Date of creation
     */
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    public Long getId() {
        return id;
    }

    public String getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(String subscriber) {
        this.subscriber = subscriber;
    }

    public NetworkOperator getNetworkOperator() {
        return networkOperator;
    }

    public void setNetworkOperator(NetworkOperator networkOperator) {
        this.networkOperator = networkOperator;
    }

    public NetworkDestinationCode getNetworkDestination() {
        return networkDestination;
    }

    public void setNetworkDestination(NetworkDestinationCode networkDestination) {
        this.networkDestination = networkDestination;
    }

    public Channel.ConnectionState getConnectionState() {
        return connectionState;
    }

    public void setConnectionState(Channel.ConnectionState connectionState) {
        this.connectionState = connectionState;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @PrePersist
    public void persist() {
        setCreatedAt(new Date());
    }
}