package com.digitalmisfits.nvs.agent.support.math;


import java.util.Random;

/**
 * Implements a <a href="http://en.wikipedia.org/wiki/Linear_feedback_shift_register">linear feedback shift register</a> (LFSR).
 * The register's size (an int named n) may be any value from 2 thru 32.
 * Any starting state of the register (called the seed) may also be specifed.
 * <p>
 * This class is not thread safe.
 * <p>
 *
 * @author Brent Boyer
 * @see <a href="https://github.com/magro/elliptic-benchmark">this github repository</a>
 */
public class PseudoRandom {

    // -------------------- fields --------------------

    private static final Random random = new Random();
    private static int count = 1;

    private final int n;
    private final int mask;
    private final int taps;
    private int register;

    /**
     * Returns an int with all the n low order bits set to 1 and all the high order bits set to 0.
     * When used as a bitwise AND mask, this will preserve the n low order bits and drop the high order bits.
     * <p>
     *
     * @throws IllegalArgumentException if n < 0 or n > 32
     */
    private static int makeMask(int n) throws IllegalArgumentException {
        if (n < 0) {
            throw new IllegalStateException("N cannot be < 0.");
        }

        if (n > 32) throw new IllegalArgumentException("n = " + n + " > 32");

        if (n == 32) return ~0;    // ~0 is thirty two 1's in binary
            /*
            The n = 32 special case must be detected for two reasons, one obvious and one subtle.

			The obvious reason is that any int that is left shifted by 32 or more ought to pushed into a long value which is impossible, so you know something weird must happen.

			The subtle reason is the details of how Java's shift operators (<<, >>, >>>) work:
			they only use the 5 lower bits of the right side operand (i.e. shift amount).
			(This statement assumes that the left hand operand is an int; if it is a long, then the lower 6 bits are used.)
			THIS MEANS THAT THEY ONLY DO WHAT YOU THINK THEY WILL WHEN THE SHIFT AMOUNT IS INSIDE THE RANGE [0, 31].
			So, in the code above, 1 << n when n = 32 evaluates to 1 << 0 (because 32 has 0 in its lower 5 bits)
			so the overall expression is then (1 << 0) - 1 == 1 - 1 == 0 which is a wrong result.

			This "use only the lower shift bits" behavior is why this code (also suggested by Sean Anderson)
				return (~0) >>> (32 - n);
			cannot be used: it fails at n = 0 (returning thirty two ones instead of 0).

			References:
				http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6201273
				http://www.davidflanagan.com/blog/000021.html
				http://java.sun.com/docs/books/jls/third_edition/html/expressions.html#15.19
			*/

        return (1 << n) - 1;    // acknowledgement: this technique sent to me by Sean Anderson, author of http://graphics.stanford.edu/~seander/bithacks.html
    }

    /**
     * Returns an int which can function as the taps of n order maximal LFSR.
     * <p>
     *
     * @throws IllegalArgumentException if n < 2 or n > 32
     */
    private static int makeTaps(int n) throws IllegalArgumentException {
        switch (n) {
            case 2:
                return (1 << 1) | (1 << 0);    // i.e. 2 1
            case 3:
                return (1 << 2) | (1 << 1);    // i.e. 3 2
            case 4:
                return (1 << 3) | (1 << 2);    // i.e. 4 3
            case 5:
                return (1 << 4) | (1 << 2);    // i.e. 5 3
            case 6:
                return (1 << 5) | (1 << 4);    // i.e. 6 5
            case 7:
                return (1 << 6) | (1 << 5);    // i.e. 7 6
            case 8:
                return (1 << 7) | (1 << 6) | (1 << 5) | (1 << 0);    // i.e. 8 7 6 1
            case 9:
                return (1 << 8) | (1 << 4);    // i.e. 9 5
            case 10:
                return (1 << 9) | (1 << 6);    // i.e. 10 7
            case 11:
                return (1 << 10) | (1 << 8);    // i.e. 11 9
            case 12:
                return (1 << 11) | (1 << 10) | (1 << 9) | (1 << 3);    // i.e. 12 11 10 4
            case 13:
                return (1 << 12) | (1 << 11) | (1 << 10) | (1 << 7);    // i.e. 13 12 11 8
            case 14:
                return (1 << 13) | (1 << 12) | (1 << 11) | (1 << 1);    // i.e. 14 13 12 2
            case 15:
                return (1 << 14) | (1 << 13);    // i.e. 15 14
            case 16:
                return (1 << 15) | (1 << 14) | (1 << 12) | (1 << 3);    // i.e. 16 15 13 4
            case 17:
                return (1 << 16) | (1 << 13);    // i.e. 17 14
            case 18:
                return (1 << 17) | (1 << 10);    // i.e. 18 11
            case 19:
                return (1 << 18) | (1 << 17) | (1 << 16) | (1 << 13);    // i.e. 19 18 17 14
            case 20:
                return (1 << 19) | (1 << 16);    // i.e. 20 17
            case 21:
                return (1 << 20) | (1 << 18);    // i.e. 21 19
            case 22:
                return (1 << 21) | (1 << 20);    // i.e. 22 21
            case 23:
                return (1 << 22) | (1 << 17);    // i.e. 23 18
            case 24:
                return (1 << 23) | (1 << 22) | (1 << 21) | (1 << 16);    // i.e. 24 23 22 17
            case 25:
                return (1 << 24) | (1 << 21);    // i.e. 25 22
            case 26:
                return (1 << 25) | (1 << 24) | (1 << 23) | (1 << 19);    // i.e. 26 25 24 20
            case 27:
                return (1 << 26) | (1 << 25) | (1 << 24) | (1 << 21);    // i.e. 27 26 25 22
            case 28:
                return (1 << 27) | (1 << 24);    // i.e. 28 25
            case 29:
                return (1 << 28) | (1 << 26);    // i.e. 29 27
            case 30:
                return (1 << 29) | (1 << 28) | (1 << 27) | (1 << 6);    // i.e. 30 29 28 7
            case 31:
                return (1 << 30) | (1 << 27);    // i.e. 31 28
            case 32:
                return (1 << 31) | (1 << 30) | (1 << 29) | (1 << 9);    // i.e. 32 31 30 10

            default:
                throw new IllegalArgumentException("n = " + n + " is an illegal value");
        }
    }

    // -------------------- constructors and helper methods --------------------

    /**
     * Convenience constructor that simply calls <code>{@link #PseudoRandom(int, int) Lfsr}(n, 1)</code>.
     * <p>
     *
     * @throws IllegalArgumentException if n < 2 or n > 32
     */
    public PseudoRandom(int n) throws IllegalArgumentException {
        this(n, 1);
    }

    /**
     * Fundamental constructor.
     * <p>
     *
     * @throws IllegalArgumentException if n < 2 or n > 32;
     *                                  if seed == 0 or has high bits (i.e. beyond the nth bit) set
     */
    public PseudoRandom(int n, int seed) throws IllegalArgumentException {
        if (n < 2) throw new IllegalArgumentException("n = " + n + " < 2");
        if (n > 32) throw new IllegalArgumentException("n = " + n + " > 32");
        if (seed == 0)
            throw new IllegalArgumentException("seed == 0; illegal because this will cause the internal state to be stuck at 0 forever");

        this.n = n;
        mask = makeMask(n);
        taps = makeTaps(n);
        register = seed;

        if ((seed & (~mask)) != 0)
            throw new IllegalArgumentException("seed = " + seed + " has high bits (i.e. beyond the nth bit) set, making it invalid state for a n = " + n + " LFSR");
    }

    // -------------------- accessors --------------------

    /**
     * Accessor for {@link #mask}.
     */
    public int getMask() {
        return mask;
    }

    /**
     * Accessor for {@link #taps}.
     */
    public int getTaps() {
        return taps;
    }

    /**
     * Accessor for {@link #register}.
     */
    public int getRegister() {
        return register;
    }

    // -------------------- advance --------------------

    /**
     * Convenience method that simply returns {@link #advance advance}(1).
     */
    public int advance() {
        return advance(1);
    }

    /**
     * Advances the internal state of this instance by numberTransitions.
     * <p>
     * Ignoring an initial argument check and a final return statement,
     * this method is essentially 2 lines of code: a loop head and its body.
     * The loop body involves 6 bitwise and/or unary int operators, <i>so it is very simple and fast</i>.
     * In spite of the simplicity of the computation, the LFSR internal state
     * (stored in the {@link #register} field) is pseudo-random.
     * It should be impossible for a smart compiler to cut many corners and avoid doing the computations.
     * <p>
     *
     * @return the final value of the internal state after all the transitions have been carried out
     * @throws IllegalArgumentException if numberTransitions < 0
     */
    public int advance(long numberTransitions) throws IllegalArgumentException {
        if (numberTransitions < 0) {
            throw new IllegalStateException("Number of transitions cannot be < 0.");
        }

        for (long i = 0; i < numberTransitions; i++) {
            register = ((register >>> 1) ^ (-(register & 1) & taps)) & mask;    // register & 1 selects the low order bit, and do - to create an int that is all 1's if was 1, or all 0's if was 0; the rest is easy to understand
        }
        return register;
    }
}
