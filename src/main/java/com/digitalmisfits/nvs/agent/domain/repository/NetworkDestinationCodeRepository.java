package com.digitalmisfits.nvs.agent.domain.repository;


import com.digitalmisfits.nvs.agent.domain.entity.NetworkDestinationCode;
import com.digitalmisfits.spring.aop.annotation.ResourceNotFound;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@ResourceNotFound
@Transactional(readOnly = true)
public interface NetworkDestinationCodeRepository extends JpaRepository<NetworkDestinationCode, Long>, JpaSpecificationExecutor<NetworkDestinationCode> {

    @Query("SELECT r.id FROM NetworkDestinationCode ndc JOIN ndc.networkOperator oper JOIN oper.destinationCodes dest JOIN dest.range r WHERE oper.id = ?1 ORDER BY RAND()")
    public List<Long> findRandomDestinationCode(long networkOperatorId, Pageable pageable);
    
    public NetworkDestinationCode findOneByNetworkOperatorAndDestinationCode(long networkOperatorId, String networkDestinationCode);
}