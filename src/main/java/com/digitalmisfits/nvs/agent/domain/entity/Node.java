package com.digitalmisfits.nvs.agent.domain.entity;

import javax.persistence.*;

@Entity
@Table(name = "nodes")
public class Node {

    @Id
    @GeneratedValue
    private Long id;

    /**
     * ISO3166 Country code
     */
    @Column(name = "iso3166", nullable = false)
    private String iso3166Code; // UG

    /**
     * Remote ip address
     */
    @Column(nullable = false, unique = true)
    private String remoteHost;

    /**
     * Remote ssh user
     */
    @Column(nullable = false)
    private String remoteUser = "openvox";

    public Long getId() {
        return id;
    }

    public String getIso3166Code() {
        return iso3166Code;
    }

    public void setIso3166Code(String iso3166Code) {
        this.iso3166Code = iso3166Code;
    }

    public String getRemoteHost() {
        return remoteHost;
    }

    public void setRemoteHost(String remoteHost) {
        this.remoteHost = remoteHost;
    }

    public String getRemoteUser() {
        return remoteUser;
    }

    public void setRemoteUser(String remoteUser) {
        this.remoteUser = remoteUser;
    }
}
