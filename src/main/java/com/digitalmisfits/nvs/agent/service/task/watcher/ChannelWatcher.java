package com.digitalmisfits.nvs.agent.service.task.watcher;


import com.digitalmisfits.nvs.agent.telephony.Channel;

public interface ChannelWatcher extends Runnable {
    
    public Channel getChannel();
}
