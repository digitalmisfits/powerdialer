package com.digitalmisfits.nvs.agent.service;


import com.digitalmisfits.nvs.agent.telephony.Channel;
import com.digitalmisfits.nvs.agent.telephony.Voucher;

public interface AirtimeService {

    public double getBalance(ChannelService channelService, Channel channel) throws Channel.OperationException;

    public double recharge(ChannelService channelService, Channel channel, Voucher voucher) throws Channel.OperationException;
}
