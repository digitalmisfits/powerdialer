package com.digitalmisfits.nvs.agent.net.cluster;

public class ClusterOperationFailureException extends RuntimeException {

    public ClusterOperationFailureException(String message) {
        super(message);
    }

    public ClusterOperationFailureException(String message, Throwable cause) {
        super(message, cause);
    }
}