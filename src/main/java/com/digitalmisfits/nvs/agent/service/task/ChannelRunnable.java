package com.digitalmisfits.nvs.agent.service.task;

import com.digitalmisfits.nvs.agent.net.Node;
import com.digitalmisfits.nvs.agent.service.*;
import com.digitalmisfits.nvs.agent.service.task.watcher.QueueChannelWatcher;
import com.digitalmisfits.nvs.agent.telephony.Channel;
import com.digitalmisfits.nvs.agent.telephony.ChannelOperator;
import com.digitalmisfits.nvs.agent.telephony.Subscriber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ChannelRunnable implements Runnable {

    private static final Logger LOG = Logger.getLogger(ChannelRunnable.class.getName());

    @Autowired
    private OperatorService provisioningService;

    @Autowired
    private TelephonyService telephonyService;

    @Autowired
    private AirtimeService airtimeService;

    @Autowired
    private VoucherService voucherService;

    /**
     * The (remote) node
     */
    private final Node node;

    /**
     * The channel to operate on
     */
    private final Channel channel;

    /**
     * Counts the number of stats
     */
    private final AtomicLong counter = new AtomicLong();

    public ChannelRunnable(Node node, Channel channel) {
        this.node = node;
        this.channel = channel;
    }

    @Override
    public void run() {

        ChannelService channelService = getChannelService(node);

        try {
            channelService.provision(channel);
            channelService.awaitChannelStateWithReset(channel, Channel.ChannelState.READY);
        } catch (Channel.OperationException e) {
            channel.setOperationalState(Channel.OperationalState.UNUSABLE);
            return;
        }

        // channel is ready. #yolo

        ChannelOperator outboundOperator = provisioningService.getChannelOperator(getNode().getIso3166CountryCode(), channel.getNetworkName());
        if (outboundOperator == null) {
            channel.setOperationalState(Channel.OperationalState.NO_OPERATOR);
            return;
        }

        channel.setChannelOperator(outboundOperator);
        channel.startChannelWatcher(getChannelWatcher(channel));
        channel.setOperationalState(Channel.OperationalState.ACTIVE);

        LOG.info(String.format("Entering main loop with channel: %s", channel));

        // main()
        while (!Thread.currentThread().isInterrupted()) { // interrupted flag is not reset

            if (channel.getBillableOperations().get() % channel.getChannelOperator().getBalanceInterval() == 0) {
                try {
                    rechargeBalanceWithRetry(channelService);
                } catch (Channel.OperationException e) {
                    LOG.info(String.format("Airtime balance/ recharge failed with '%s'", e.getMessage()));
                    channel.setOperationalState(Channel.OperationalState.NO_AIRTIME);
                    break;
                }
            }

            try {
                Subscriber subscriber = channel.getInboundQueue().poll(1, TimeUnit.MINUTES);

                if (subscriber == null)
                    continue; // poll expired. continue loop, allowing maintenance tasks to run

                channelService.awaitChannelStateWithReset(channel, Channel.ChannelState.READY);

                // channel is READY
                Channel.ConnectionState state = channelService.stat(channel, String.format("%s%s", outboundOperator.getIdp(), subscriber.getSubscriber()));
                subscriber.setNetworkOperatorId(outboundOperator.getId());

                LOG.info(String.format("Channel %d is ready. Number stat (%s) returned %s", channel.getSpan(), subscriber.getSubscriber(), state));

                // channel is in use (inbound call/ inbound sms, etc.)
                if (state == Channel.ConnectionState.USING) {
                    LOG.info(String.format("ConnectionState on channel %d is %s", channel.getSpan(), state));

                    if (channel.getChannelState() == Channel.ChannelState.PHONE_CHECK) {
                        LOG.fine(String.format("ChannelState on channel %d is %s. Stuck? Interrupting", channel.getSpan(), channel.getChannelState()));

                        channelService.interruptChannel(channel, 10, TimeUnit.SECONDS);
                    }

                    continue;
                }

                telephonyService.store(subscriber, state);

            } catch (Channel.OperationInterruptedException e) {
                break;
            } catch (Channel.OperationException e) {
                LOG.info(String.format("OperationException thrown with %s", e.getMessage()));
            } catch (InterruptedException e) {
                break;
            }
        }

        stopChannelWatcher();
    }

    /**
     * Request the span/network operator balance and recharge is the set threshold exceeds the balance
     *
     * @param channelService
     * @return
     * @throws Channel.OperationException
     */

    private boolean rechargeBalanceWithRetry(ChannelService channelService) throws Channel.OperationException {

        channelService.awaitChannelStateWithReset(channel, Channel.ChannelState.READY);

        try {
            channel.setChannelOperatorBalance(airtimeService.getBalance(channelService, channel));

            if (channel.getChannelOperatorBalance() <= channel.getChannelOperator().getBalanceThreshold()) {
                channel.setChannelOperatorBalance(airtimeService.recharge(channelService, channel, voucherService.findVoucher(channel.getChannelOperator())));
                return true;
            }
        } catch (VoucherService.VoucherNotAvailableException e) {
            throw new Channel.OperationException(e.getMessage(), e);
        }

        return false;
    }

    /**
     * Stop the channel watcher on the current channel
     */
    private void stopChannelWatcher() {

        try {
            channel.stopChannelWatcher();
        } catch (InterruptedException e) {
            // void
        }
    }

    @Lookup
    public QueueChannelWatcher getChannelWatcher(Channel channel) {
        throw new IllegalStateException("Method not instrumented");
    }

    @Lookup
    public ChannelService getChannelService(Node node) {
        throw new IllegalStateException("Method not instrumented");
    }

    public Node getNode() {
        return node;
    }

    public Channel getChannel() {
        return channel;
    }
}
