package com.digitalmisfits.nvs.agent.service.task;


import com.digitalmisfits.nvs.agent.net.Node;
import com.digitalmisfits.nvs.agent.service.NodeProvisioningService;
import com.digitalmisfits.nvs.agent.telephony.Channel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.concurrent.Callable;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class NodeCallable implements Callable<Collection<Channel>> {

    @Autowired
    private NodeProvisioningService nodeProvisioningService;

    /**
     * The node
     */
    private final Node node;

    public NodeCallable(Node node) {
        this.node = node;
    }

    @Override
    public Collection<Channel> call() throws Exception {
        
        return nodeProvisioningService.queryChannels(node);
    }
}
