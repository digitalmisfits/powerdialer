package com.digitalmisfits.nvs.agent.telephony.ussd;

public interface USSDAirtimeConfiguration {

    /**
     * Denotes a general ussd operation runtime exception
     */
    public static class ConfigurationNotFoundException extends RuntimeException {

        public ConfigurationNotFoundException(String message) {
            super(message);
        }

        public ConfigurationNotFoundException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    public String getISO3166Code();

    public String getNetworkName();

    public String getBalanceCode();

    public double getBalance(String input);

    public String getRechargeCode();

    public double getRecharge(String input);
}
