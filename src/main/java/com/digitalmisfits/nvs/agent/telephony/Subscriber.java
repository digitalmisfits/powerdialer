package com.digitalmisfits.nvs.agent.telephony;

public class Subscriber {

    /**
     * The origination network operator
     */
    private long networkOperatorId;

    /**
     * The termination network destination
     */
    private long networkDestinationId;

    /**
     * Subscriber (extension)
     */
    private String subscriber;

    /**
     * The connection state
     */
    private Channel.ConnectionState connectionState = Channel.ConnectionState.UNKNOWN;

    public Subscriber(long networkDestinationId, String subscriber) {
        this.networkDestinationId = networkDestinationId;
        this.subscriber = subscriber;
    }

    public long getNetworkOperatorId() {
        return networkOperatorId;
    }

    public void setNetworkOperatorId(long networkOperatorId) {
        this.networkOperatorId = networkOperatorId;
    }

    public long getNetworkDestinationId() {
        return networkDestinationId;
    }

    public String getSubscriber() {
        return subscriber;
    }

    public Channel.ConnectionState getConnectionState() {
        return connectionState;
    }

    public void setConnectionState(Channel.ConnectionState connectionState) {
        this.connectionState = connectionState;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Phonenumber{");
        sb.append("networkOperatorId=").append(networkOperatorId);
        sb.append(", networkDestinationId=").append(networkDestinationId);
        sb.append(", subscriber='").append(subscriber).append('\'');
        sb.append(", connectionState=").append(connectionState);
        sb.append('}');
        return sb.toString();
    }
}
