package com.digitalmisfits.nvs.agent.domain.repository;


import com.digitalmisfits.nvs.agent.domain.entity.NetworkDestinationRange;
import com.digitalmisfits.spring.aop.annotation.ResourceNotFound;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.LockModeType;

@Repository
@ResourceNotFound
@Transactional(readOnly = true)
public interface NetworkDestinationRangeRepository extends JpaRepository<NetworkDestinationRange, Long>, JpaSpecificationExecutor<NetworkDestinationRange> {

    @Transactional(readOnly = false)
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    public NetworkDestinationRange findOne(Long id);
}