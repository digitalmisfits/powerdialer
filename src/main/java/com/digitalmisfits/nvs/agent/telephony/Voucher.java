package com.digitalmisfits.nvs.agent.telephony;

import javax.validation.constraints.Null;
import java.util.Date;

public class Voucher {
    
    @Null
    private Date expires;
    
    private String accessNumber;

    public Voucher(String accessNumber) {
        this.accessNumber = accessNumber;
    }

    public Date getExpires() {
        return expires;
    }

    public void setExpires(Date expires) {
        this.expires = expires;
    }

    public String getAccessNumber() {
        return accessNumber;
    }

    public void setAccessNumber(String accessNumber) {
        this.accessNumber = accessNumber;
    }
}
