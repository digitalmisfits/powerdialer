package com.digitalmisfits.nvs.agent.domain.service;

import com.digitalmisfits.nvs.agent.domain.entity.NetworkDestinationRange;
import com.digitalmisfits.nvs.agent.domain.entity.NetworkOperator;
import com.digitalmisfits.nvs.agent.domain.entity.Phonenumber;
import com.digitalmisfits.nvs.agent.domain.repository.NetworkDestinationCodeRepository;
import com.digitalmisfits.nvs.agent.domain.repository.NetworkDestinationRangeRepository;
import com.digitalmisfits.nvs.agent.domain.repository.NetworkOperatorRepository;
import com.digitalmisfits.nvs.agent.domain.repository.PhonenumberRepository;
import com.digitalmisfits.nvs.agent.service.TelephonyService;
import com.digitalmisfits.nvs.agent.support.math.PseudoRandom;
import com.digitalmisfits.nvs.agent.telephony.Channel;
import com.digitalmisfits.nvs.agent.telephony.ChannelOperator;
import com.digitalmisfits.nvs.agent.telephony.Subscriber;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;
import java.util.Collection;
import java.util.logging.Logger;

@Service
public class JpaTelephonyService implements TelephonyService {

    private final Logger logger = Logger.getLogger(JpaTelephonyService.class.getName());

    @Autowired
    private NetworkOperatorRepository networkOperatorRepository;

    @Autowired
    private NetworkDestinationCodeRepository networkDestinationRepository;

    @Autowired
    private NetworkDestinationRangeRepository rangeRepository;

    @Autowired
    private PhonenumberRepository phonenumberRepository;

    @Transactional(Transactional.TxType.REQUIRED)
    public Collection<Subscriber> load(ChannelOperator channelOperator, int results) {

        NetworkOperator networkOperator;

        try {
            networkOperator = networkOperatorRepository.findWeightOrderedOperators(channelOperator.getId(), new PageRequest(0, 1)).get(0);
        } catch (NoResultException e) {
            return ImmutableSet.of();
        }

        logger.info(String.format("Loading numbers from operator %s", networkOperator.getName()));

        Long rangeId;

        try {
            rangeId = networkDestinationRepository.findRandomDestinationCode(networkOperator.getId(), new PageRequest(0, 1)).get(0);
        } catch (NoResultException e) {
            return ImmutableSet.of();
        }

        NetworkDestinationRange range;

        try {
            range = rangeRepository.findOne(rangeId); // lock mode = pessimistic
        } catch (NoResultException e) {
            return ImmutableSet.of();
        }

        logger.info(String.format("Loading destination code %s from operator %s", range.getDestinationCode().getDestinationCode(), networkOperator.getName()));

        ImmutableSet.Builder<Subscriber> output = new ImmutableSet.Builder<>();

        // range identifier
        Range<Integer> r = Range.open(range.getStart(), range.getEnd());

        // LFSR random number generator
        PseudoRandom random = new PseudoRandom(range.getRegisterSize(), range.getSeed());

        // generate a maximum of [results] numbers
        while (results-- > 0) {

            if (range.getState() != NetworkDestinationRange.RangeState.EXHAUSTED) {
                range.setSeed(random.advance());

                if (r.contains(range.getSeed())) {

                    String s = String.format("%s%s%s",
                            networkOperator.getE164CountryCode(),
                            range.getDestinationCode().getDestinationCode(),
                            Strings.padStart(String.valueOf(range.getSeed()), range.getLength(), '0'));

                    output.add(new Subscriber(range.getDestinationCode().getId(), s));
                }

            } else {
                break;
            }
        }

        rangeRepository.save(range);
        
        /*
            select *, -LOG(RAND()) / weight as priority
                from dialplan dp
                    inner join dpconnector dpc on (dp.id = dpc.dialplan_id)
                    inner join operators oper on (dpc.terminator_id = oper.id)
                    inner join destinations dest on (oper.id = dest.id)
                    inner join ranges on (dest.range_id = ranges.id)

                where originator_id = 1
                ORDER BY priority limit 1
            for update
         */

        return output.build();
    }

    @Override
    public void store(Subscriber subscriber, Channel.ConnectionState state) {

        Phonenumber p = new Phonenumber();
        p.setNetworkOperator(networkOperatorRepository.findOne(subscriber.getNetworkOperatorId()));
        p.setNetworkDestination(networkDestinationRepository.findOne(subscriber.getNetworkDestinationId()));
        p.setSubscriber(subscriber.getSubscriber());
        p.setConnectionState(state);

        phonenumberRepository.saveAndFlush(p);
    }
}
