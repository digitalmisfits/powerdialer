package com.digitalmisfits.nvs.agent.service;


import com.digitalmisfits.nvs.agent.telephony.ChannelOperator;
import com.digitalmisfits.nvs.agent.telephony.Voucher;

public interface VoucherService {

    class VoucherNotAvailableException extends Exception {

        public VoucherNotAvailableException() {
        }
    }

    Voucher findVoucher(ChannelOperator channelOperator) throws VoucherNotAvailableException;
}
