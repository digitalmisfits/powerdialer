var app = angular.module('app', [
    'ngRoute',
    'ui.bootstrap',
    'agentControllers'
]);

app.config(["$httpProvider", '$routeProvider',
    function ($httpProvider, $routeProvider) {

        $httpProvider.defaults.headers.post['X-CSRF-TOKEN'] = document.querySelector("meta[name=csrf]").getAttribute("content");

        $routeProvider.
            when('/', {
                templateUrl: 'partials/home.html',
                label: 'Home'
            }).
            when('/nodes', {
                templateUrl: 'partials/nodes-list.html',
                controller: 'NodeListCtrl'
            }).
            when('/node/:nodeId', {
                templateUrl: 'partials/nodes-details.html',
                controller: 'NodeDetailsCtrl'
            }).
            when('/channels', {
                templateUrl: 'partials/channels-list.html',
                controller: 'ChannelListCtrl'
            }).
            when('/operators', {
                templateUrl: 'partials/operators-list.html',
                controller: 'OperatorListCtrl'
            }).
            when('/operator/:operatorId', {
                templateUrl: 'partials/operators-details.html',
                controller: 'OperatorDetailsCtrl'
            }).
            when('/operator/:operatorId/destinations/:destinationId', {
                templateUrl: 'partials/destination-details.html',
                controller: 'DestinationDetailsCtrl'
            }).
            when('/dialplans', {
                templateUrl: 'partials/dialplans-list.html',
                controller: 'DialplanListCtrl'
            }).
            otherwise({
                redirectTo: '/'
            });
    }
]);