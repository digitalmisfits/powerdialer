package com.digitalmisfits.nvs.agent.telephony.ussd.uganda;

import com.digitalmisfits.nvs.agent.telephony.ussd.USSDAirtimeConfiguration;
import com.google.common.base.Preconditions;

import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CelTel implements USSDAirtimeConfiguration {

    private static final Logger LOG = Logger.getLogger(CelTel.class.getName());

    private static final Pattern BALANCE;

    static {
        // @formatter:off
        BALANCE     = Pattern.compile("Acct bal is Ush (?<balance>\\d+)");
        // @formatter:on
    }

    @Override
    public String getISO3166Code() {
        return "UG";
    }

    @Override
    public String getNetworkName() {
        return "CelTel Cellular";
    }

    @Override
    public String getBalanceCode() {
        return "*131";
    }

    @Override
    public double getBalance(String input) {

        Matcher m = BALANCE.matcher(Preconditions.checkNotNull(input));
        if (m.find()) {
            return Double.valueOf(m.group("balance"));
        } else {
            LOG.severe(String.format("Unable to capture balance from input %s", input));
        }

        return -1;
    }

    @Override
    public String getRechargeCode() {
        return "*130*";
    }

    @Override
    public double getRecharge(String input) {
        throw new UnsupportedOperationException();
    }
}

