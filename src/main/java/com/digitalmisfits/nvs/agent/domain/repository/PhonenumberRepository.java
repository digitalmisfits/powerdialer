package com.digitalmisfits.nvs.agent.domain.repository;

import com.digitalmisfits.nvs.agent.domain.entity.Phonenumber;
import com.digitalmisfits.spring.aop.annotation.ResourceNotFound;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@ResourceNotFound
@Transactional(readOnly = true)
public interface PhonenumberRepository extends JpaRepository<Phonenumber, Long>, JpaSpecificationExecutor<Phonenumber> {


}