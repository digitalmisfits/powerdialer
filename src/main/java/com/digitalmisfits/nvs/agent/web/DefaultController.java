package com.digitalmisfits.nvs.agent.web;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.logging.Logger;

@Controller
public class DefaultController {

    private final Logger logger = Logger.getLogger(DefaultController.class.getName());

    @RequestMapping(value = "/robots.txt", method = RequestMethod.GET, produces = "text/plain")
    @ResponseBody
    public String robots() {
        return "User-agent: *\nDisallow: /\n";
    }

    @RequestMapping(value = {"/", "/index.html"}, method = RequestMethod.GET)
    public String main() {
        return "index";
    }
}


