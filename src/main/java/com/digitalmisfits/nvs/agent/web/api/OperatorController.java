package com.digitalmisfits.nvs.agent.web.api;

import com.digitalmisfits.nvs.agent.domain.dto.NetworkOperatorDTO;
import com.digitalmisfits.nvs.agent.domain.repository.NetworkOperatorRepository;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class OperatorController {

    @Autowired
    private NetworkOperatorRepository networkOperatorRepository;

    @Autowired
    private MapperFacade mapper;

    @RequestMapping(value = "/operators", method = RequestMethod.GET)
    public Map<String, List<NetworkOperatorDTO>> operators() {

        return networkOperatorRepository.findAll()
                .stream()
                .map(networkOperator -> mapper.map(networkOperator, NetworkOperatorDTO.class))
                .collect(Collectors.groupingBy(NetworkOperatorDTO::getIso3166CountryCode));
    }

    @RequestMapping(value = "/operator/{operatorId}", method = RequestMethod.GET)
    public NetworkOperatorDTO operatorId(@PathVariable long operatorId) {

        return mapper.map(networkOperatorRepository.findOne(operatorId), NetworkOperatorDTO.class);
    }

    @RequestMapping(value = "/operator/{operatorId}", method = RequestMethod.POST)
    public NetworkOperatorDTO save(@PathVariable long operatorId, @Valid @RequestBody NetworkOperatorDTO networkOperatorDTO) {

        return mapper.map(networkOperatorRepository.findOne(operatorId), NetworkOperatorDTO.class);
    }
}
