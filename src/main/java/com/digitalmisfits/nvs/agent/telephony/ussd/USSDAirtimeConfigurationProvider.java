package com.digitalmisfits.nvs.agent.telephony.ussd;

import com.google.common.collect.ImmutableSet;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.env.Environment;
import org.springframework.core.type.filter.AssignableTypeFilter;
import org.springframework.stereotype.Component;

import java.lang.reflect.Constructor;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

/**
 * This provider class provides different USSDAirtimeConfiguration implementation instances based on
 * the ISO 3166 country code and the network name.
 * <p>
 * After construction, this factory scans the airtime.configuration.basePackage environment property
 * in order to discover implementations.
 */
@Component
public class USSDAirtimeConfigurationProvider implements InitializingBean {

    private static final Logger LOG = Logger.getLogger(USSDAirtimeConfigurationProvider.class.getName());

    @Autowired
    Environment env;

    private final Map<ImmutableSet<String>, USSDAirtimeConfiguration> configurations = new ConcurrentHashMap<>();

    @Override
    public void afterPropertiesSet() {
        LOG.info("Initializing " + this.getClass().getName());

        ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(false);
        provider.addIncludeFilter(
                new AssignableTypeFilter(USSDAirtimeConfiguration.class)
        );

        String basePackage = env.getProperty("airtime.configuration.basePackages", Marker.class.getPackage().getName());

        for (BeanDefinition component : provider.findCandidateComponents(basePackage)) {

            LOG.info(String.format("Found candidate component %s", component.getBeanClassName()));

            try {
                Class<?> cls = Class.forName(component.getBeanClassName());
                Constructor<?> constructor = cls.getConstructor();
                USSDAirtimeConfiguration config = (USSDAirtimeConfiguration) constructor.newInstance();

                configurations.put(ImmutableSet.of(config.getISO3166Code(), config.getNetworkName()), config);

            } catch (ReflectiveOperationException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        }
    }

    /**
     * Lookup a USSDAirtimeConfiguration implementation based on the ISO3166 country code and network name
     *
     * @param iso3166code The ISO3166 country code
     * @param networkName The network name
     * @return
     */
    public USSDAirtimeConfiguration lookup(String iso3166code, String networkName) {

        USSDAirtimeConfiguration conf = configurations.get(ImmutableSet.of(iso3166code, networkName));

        if (conf == null) {
            throw new USSDAirtimeConfiguration.ConfigurationNotFoundException(
                    String.format("USSDAirtimeConfiguration %s/%s not found", iso3166code, networkName));
        }

        return conf;
    }
}