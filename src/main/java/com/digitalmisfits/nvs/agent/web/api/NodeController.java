package com.digitalmisfits.nvs.agent.web.api;

import com.digitalmisfits.nvs.agent.domain.dto.ServerNodeDTO;
import com.digitalmisfits.nvs.agent.domain.repository.NodeRepository;
import com.digitalmisfits.nvs.agent.net.Node;
import com.digitalmisfits.nvs.agent.net.NodeGroup;
import com.digitalmisfits.nvs.agent.telephony.Channel;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class NodeController {

    @Autowired
    private NodeGroup nodeGroup;
    
    @Autowired
    private NodeRepository nodeRepository;

    @Autowired
    private MapperFacade mapper;

    @RequestMapping(value = "/nodes", method = RequestMethod.GET)
    public Map<String, List<ServerNodeDTO>> nodes() {
        
        return nodeRepository.findAll()
                .stream()
                .map(node -> mapper.map(node, ServerNodeDTO.class))
                .collect(Collectors.groupingBy(ServerNodeDTO::getIso3166Code));
    }

    @RequestMapping(value = "/node/{id}", method = RequestMethod.GET)
    public Node getNode(@PathVariable("id") long id) {
        return nodeGroup.query(id);
    }

    @RequestMapping(value = "/node/{id}/channels", method = RequestMethod.GET)
    public Collection<Channel> getNodeChannels(@PathVariable("id") long id) {
        return nodeGroup.query(id).getChannelGroup().getAllChannels();
    }
}

