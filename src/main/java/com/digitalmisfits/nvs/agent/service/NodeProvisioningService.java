package com.digitalmisfits.nvs.agent.service;

import com.digitalmisfits.nvs.agent.net.Node;
import com.digitalmisfits.nvs.agent.telephony.Channel;
import com.google.common.collect.ImmutableSet;

public interface NodeProvisioningService {

    public ImmutableSet<Channel> queryChannels(Node node)
        throws Channel.OperationException;
}
