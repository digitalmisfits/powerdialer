package com.digitalmisfits.nvs.agent.config;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import retrofit.Endpoint;
import retrofit.Endpoints;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.util.concurrent.TimeUnit;

@Configuration
public class WebServicesConfig {

    @Autowired
    Environment environment;

    @Bean
    public OkHttpClient provideOkHttpClient() {

        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setConnectTimeout(30, TimeUnit.SECONDS);
        okHttpClient.setWriteTimeout(30, TimeUnit.SECONDS);
        okHttpClient.setReadTimeout(30, TimeUnit.SECONDS);

        return okHttpClient;
    }

    @Bean
    public Endpoint localeAwareProvisioningEndpoint() {

        return Endpoints.newFixedEndpoint(environment.getProperty("http.provisioning.endpoint"));
    }

    /*
    @Bean
    public ProvisioningApi provisioningApi(Endpoint endpoint, OkHttpClient okHttp, Gson gson) {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(localeAwareProvisioningEndpoint())

                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String message) {
                        Logger.getLogger("ProvisioningApi").info(message);
                    }
                })
                .setErrorHandler(new RetrofitExceptionTranslator())
                .setClient(new OkClient(okHttp))
                .setConverter(new GsonConverter(gsonSerializer()))
                .setExecutors(Executors.newCachedThreadPool(), null)
                .build();

        return restAdapter.create(ProvisioningApi.class);
    }
*/
    @Bean
    public Gson gsonSerializer() {
        return new Gson();
    }

    @Bean
    public ServletContextInitializer servletContextInitializer() {
        return new ServletContextInitializer() {

            @Override
            public void onStartup(ServletContext servletContext) throws ServletException {
                servletContext.getSessionCookieConfig().setName("sid");
            }
        };
    }
}
