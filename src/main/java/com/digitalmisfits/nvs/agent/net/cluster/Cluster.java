package com.digitalmisfits.nvs.agent.net.cluster;


import com.google.common.collect.ImmutableSet;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.imps.CuratorFrameworkState;
import org.apache.curator.framework.recipes.cache.PathChildrenCache;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheListener;
import org.apache.curator.framework.state.ConnectionState;
import org.apache.curator.framework.state.ConnectionStateListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.SmartLifecycle;

import java.io.Closeable;
import java.io.IOException;

public class Cluster implements ConnectionStateListener, PathChildrenCacheListener, Closeable, SmartLifecycle {

    protected static final Logger LOG = LoggerFactory.getLogger(Cluster.class);

    /**
     * The Apache Zookeeper Curator framework client
     */
    private final CuratorFramework curatorFramework;

    /**
     * The ClusterInstance (e.a. this node) to be registered with ZooKeeper
     */
    private final ClusterInstance clusterInstance;

    /**
     * The default path (absolute parent) to node instances in ZooKeeper
     */
    private String path = "/nodes";

    /**
     * Determines if this service is currently running
     */
    private boolean isRunning = false;

    /**
     * Determines if this service is automatically started
     */
    private boolean isAutoStartup = false;

    public Cluster(CuratorFramework curatorFramework, ClusterInstance clusterInstance) {
        this.curatorFramework = curatorFramework;
        this.clusterInstance = clusterInstance;
    }

    @Override
    public boolean isAutoStartup() {
        return isAutoStartup;
    }

    public void setAutoStartup(boolean isAutoStartup) {
        this.isAutoStartup = isAutoStartup;
    }

    /**
     * Start the cluster. Most mutator methods will not work until the cluster is started
     *
     * @throws ClusterOperationFailureException
     */
    @Override
    public void start() {

        LOG.info("Starting " + this.getClass().getName());

        if (curatorFramework.getState() != CuratorFrameworkState.LATENT) {
            throw new ClusterOperationFailureException("CuratorFramework cannot be manually started.");
        }

        curatorFramework.start();

        try {
            createPathChildrenCache().start(PathChildrenCache.StartMode.NORMAL);
        } catch (Exception e) {

            LOG.error("PathChildrenCache could not be started. Closing CuratorFramework connection");

            try {
                close();
            } catch (IOException ignore) {
                // meh
            }

            throw new ClusterOperationFailureException("PathChildrenCache could no be started.");
        }
    }

    @Override
    public void stop(Runnable callback) {
        stop();
        callback.run();
    }

    @Override
    public void stop() {
        try {
            close();
        } catch (IOException e) {
            // ignore
        }
    }

    @Override
    public boolean isRunning() {
        return isRunning;
    }

    @Override
    public int getPhase() {
        return Integer.MIN_VALUE;
    }

    protected PathChildrenCache createPathChildrenCache() {

        PathChildrenCache cache = new PathChildrenCache(curatorFramework, path, false);
        cache.getListenable().addListener(this);
        return cache;
    }

    @Override
    public void stateChanged(CuratorFramework curatorFramework, ConnectionState state) {
        // ignore
    }

    @Override
    public void childEvent(CuratorFramework curatorFramework, PathChildrenCacheEvent pathChildrenCacheEvent) throws Exception {
        LOG.info("PathChildrenCacheEvent received: {}", pathChildrenCacheEvent.getType());

        if (ImmutableSet.of(
                PathChildrenCacheEvent.Type.CHILD_ADDED,
                PathChildrenCacheEvent.Type.CHILD_REMOVED,
                PathChildrenCacheEvent.Type.INITIALIZED
        ).stream().anyMatch(e -> e == pathChildrenCacheEvent.getType())) {
            // propagate event
        }

    }

    @Override
    public void close() throws IOException {
        if (curatorFramework.getState() == CuratorFrameworkState.STARTED)
            curatorFramework.close();
    }
}
