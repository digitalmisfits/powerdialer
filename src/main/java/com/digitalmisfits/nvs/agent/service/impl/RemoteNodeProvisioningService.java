package com.digitalmisfits.nvs.agent.service.impl;

import com.digitalmisfits.nvs.agent.net.Node;
import com.digitalmisfits.nvs.agent.service.ChannelService;
import com.digitalmisfits.nvs.agent.service.NodeProvisioningService;
import com.digitalmisfits.nvs.agent.telephony.Channel;
import com.google.common.collect.ImmutableSet;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Service;

@Service
public class RemoteNodeProvisioningService implements NodeProvisioningService {

    @Override
    public ImmutableSet<Channel> queryChannels(Node node)
            throws Channel.OperationException {

        ImmutableSet<Channel> channels;

        try {
            channels = getRemoteChannelService(node).queryChannels();
        } catch (Channel.OperationException e) {
            throw e;
        }

        return channels;
    }

    @Lookup
    public ChannelService getRemoteChannelService(Node node) {
        throw new UnsupportedOperationException("Method not instrumented");
    }
}
