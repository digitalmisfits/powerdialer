package com.digitalmisfits.spring.aop.data.repository;


import com.digitalmisfits.spring.aop.annotation.ResourceNotFound;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Collection;

/**
 * This Aspect "traps" any non-void public method on repository definitions with either an ResourceNotFound at class level
 * or on method level (to be more specific or to only trap certain methods)
 * If the aspect has been trapped, it will inspect the return value and when its either null or an empty collection
 * it will throw a ResourceNotFound Exception (with locale information)
 *
 * @author epdittmer@digitalmisfits.com
 */
@Aspect
@Order(Ordered.HIGHEST_PRECEDENCE)
@Component
public class ResourceNotFoundAspect {

    private static Logger log = LoggerFactory.getLogger(ResourceNotFoundAspect.class);

    @SuppressWarnings("unchecked")
    public static boolean isObjectEmpty(Object object) {

        if (object == null) {
            return true;
        } else if (object instanceof Collection<?>) {
            return ((Collection<?>) object).isEmpty();
        }

        return false;
    }

    //@Pointcut("within(@org.springframework.stereotype.Controller *)")
    @Pointcut("execution(@org.springframework.stereotype.Controller * *(..)) && @annotation(org.springframework.stereotype.Controller)")
    public void classPointcut() {
    }

    @Around("publicNonVoidRepositoryMethod()")
    public Object publicNonVoidRepositoryMethod(ProceedingJoinPoint pjp) throws Throwable {

        Object retVal = pjp.proceed();

        ResourceNotFound methodAnnotation = ((MethodSignature) pjp.getSignature()).getMethod()
                .getAnnotation(ResourceNotFound.class);

        boolean hasClassAnnotation = false;
        for (Class<?> i : pjp.getTarget().getClass().getInterfaces()) {
            if (i.getAnnotation(ResourceNotFound.class) != null) {
                hasClassAnnotation = true;
                break;
            }
        }

        if (hasClassAnnotation && isObjectEmpty(retVal))
            throw new RuntimeException("Resource not found");

        return retVal;
    }
}
