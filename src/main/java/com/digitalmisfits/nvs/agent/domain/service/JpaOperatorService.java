package com.digitalmisfits.nvs.agent.domain.service;


import com.digitalmisfits.nvs.agent.domain.entity.NetworkOperator;
import com.digitalmisfits.nvs.agent.domain.repository.NetworkOperatorRepository;
import com.digitalmisfits.nvs.agent.service.OperatorService;
import com.digitalmisfits.nvs.agent.service.TelephonyService;
import com.digitalmisfits.nvs.agent.telephony.ChannelOperator;
import com.digitalmisfits.nvs.agent.telephony.Subscriber;
import com.google.common.collect.ImmutableSet;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.logging.Logger;

@Service
public class JpaOperatorService implements OperatorService {

    private static final Logger log = Logger.getLogger(JpaOperatorService.class.getName());

    @Autowired
    private NetworkOperatorRepository operatorRepository;

    @Autowired
    private TelephonyService telephonyService;

    @Autowired
    private MapperFacade mapper;

    @Override
    public Collection<ChannelOperator> queryChannelOperators() {

        final ImmutableSet.Builder<ChannelOperator> operators = new ImmutableSet.Builder<>();

        operatorRepository.findAll().forEach((networkOperator) -> {
            operators.add(fromNetworkOperator(networkOperator));
        });

        return operators.build();
    }

    @Override
    public ChannelOperator getChannelOperator(String country, String networkName) {

        NetworkOperator networkOperator = operatorRepository.findGeographicalOperator(country, networkName);

        if (networkOperator != null) {
            return fromNetworkOperator(networkOperator);
        }

        return null;
    }

    private ChannelOperator fromNetworkOperator(NetworkOperator o) {
        return mapper.map(o, ChannelOperator.class);
    }

    @Override
    public Collection<Subscriber> getNumbers(ChannelOperator channelOperator) {

        return telephonyService.load(channelOperator, 10);
    }
}
