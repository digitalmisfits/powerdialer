package com.digitalmisfits.nvs.agent.domain.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "vouchers")
public class AirtimeVoucher {

    @Id
    @GeneratedValue
    private Long id;

    /**
     * The (most of the time 16 digit) voucher number
     */
    @Column(nullable = false)
    private String number;

    /**
     * Determines whether this voucher is available for redemption
     */
    @Column(nullable = false)
    private boolean isAvailable = true;

    /**
     * Voucher creation date
     */
    @Column
    @Temporal(TemporalType.DATE)
    private Date createdAt;

    /**
     * Many-to-One mapping to the network operator
     */
    @ManyToOne
    @JoinColumn(name="operator_id",nullable = false)
    private NetworkOperator networkOperator;

    public Long getId() {
        return id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public NetworkOperator getNetworkOperator() {
        return networkOperator;
    }

    public void setNetworkOperator(NetworkOperator networkOperator) {
        this.networkOperator = networkOperator;
    }
}
