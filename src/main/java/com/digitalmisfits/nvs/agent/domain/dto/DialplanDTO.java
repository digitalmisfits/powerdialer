package com.digitalmisfits.nvs.agent.domain.dto;

import java.util.ArrayList;
import java.util.List;

public class DialplanDTO {

    public String name;

    public int weight;

    public List<DialplanNetworkConnection> connections = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public List<DialplanNetworkConnection> getConnections() {
        return connections;
    }

    public void setConnections(List<DialplanNetworkConnection> connections) {
        this.connections = connections;
    }

    public static class DialplanNetworkConnection {

        public String originationNetworkOperator;

        public String terminationNetworkOperator;

        public DialplanNetworkConnection(String originationNetworkOperator, String terminationNetworkOperator) {
            this.originationNetworkOperator = originationNetworkOperator;
            this.terminationNetworkOperator = terminationNetworkOperator;
        }

        public String getOriginationNetworkOperator() {
            return originationNetworkOperator;
        }

        public void setOriginationNetworkOperator(String originationNetworkOperator) {
            this.originationNetworkOperator = originationNetworkOperator;
        }

        public String getTerminationNetworkOperator() {
            return terminationNetworkOperator;
        }

        public void setTerminationNetworkOperator(String terminationNetworkOperator) {
            this.terminationNetworkOperator = terminationNetworkOperator;
        }
    }
}
