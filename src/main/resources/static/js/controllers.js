var agentControllers = angular.module('agentControllers', []);

/**
 * Node List Controller
 */
agentControllers.controller('NodeListCtrl', ['$scope', '$location', '$http',
    function ($scope, $location, $http) {
        
        $http.get('nodes').success(function(data) {
            $scope.countries = data;
        });

        $scope.details = function ( path ) {
            $location.path( path );
        };
    }
]);

/**
 * Node Detail Controller
 */
agentControllers.controller('NodeDetailsCtrl', ['$scope', '$routeParams', '$http',
    function ($scope, $routeParams, $http) {

        $scope.nodeId = $routeParams.nodeId;

        $http.get('node/' + $scope.nodeId).success(function(data) {
            $scope.node = data;
        });
    }
]);

/**
 * Channel List Controller
 */
agentControllers.controller('ChannelListCtrl', ['$scope', '$location', '$http',
    function ($scope, $location, $http) {

        $http.get('channels').success(function(data) {
            $scope.nodes = data;
        });
    }
]);

/**
 * Operator List Controller
 */
agentControllers.controller('OperatorListCtrl', ['$scope', '$location', '$http',
    function ($scope, $location, $http) {

        $http.get('operators').success(function(data) {
            $scope.countries = data;
        });

        $scope.details = function ( path ) {
            $location.path( path );
        };

        $scope.isCollapsed = true;
    }
]);

/**
 *  Operator Details Controller
 */
agentControllers.controller('OperatorDetailsCtrl', ['$scope', '$routeParams', '$http',
    function ($scope, $routeParams, $http) {

        $scope.operatorId = $routeParams.operatorId;

        $http.get('operator/' + $scope.operatorId).success(function(data) {
            $scope.operator = data;
        });

        $scope.save = function () {
            $http.post('operator/' + $scope.operatorId, $scope.operator).
                success(function (data, status) {
                    // ignore
                }
            );
        }
    }
]);

/**
 * Dialplans List Controller
 */
agentControllers.controller('DialplanListCtrl', ['$scope', '$location', '$http',
    function ($scope, $location, $http) {

        $http.get('dialplans').success(function(data) {
            $scope.dialplans = data;
    });

        $scope.details = function ( path ) {
            $location.path( path );
        };
    }
]);
