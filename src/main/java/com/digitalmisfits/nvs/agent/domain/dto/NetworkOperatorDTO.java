package com.digitalmisfits.nvs.agent.domain.dto;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

public class NetworkOperatorDTO {

    private Long id;

    @NotNull
    @NotEmpty
    private String iso3166CountryCode;

    @NotNull
    @NotEmpty
    private String name;

    @NotNull
    @NotEmpty
    private String networkName;

    @NotNull
    @NotEmpty
    private String e164CountryCode;

    @NotNull
    @NotEmpty
    private String idp;

    private boolean hasCallBarringCapability = true; // default operator allows call barring

    private boolean hasUUSDCapability;

    private boolean isOutboundEnabled;

    @NotNull
    private int permitsPerSecond;

    @NotNull
    @Min(1)
    private double balanceThreshold;

    @NotNull
    @Min(25)
    private int balanceInterval;

    private List<NetworkDestinationDTO> networkDestinationCodes;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getIso3166CountryCode() {
        return iso3166CountryCode;
    }

    public void setIso3166CountryCode(String iso3166CountryCode) {
        this.iso3166CountryCode = iso3166CountryCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNetworkName() {
        return networkName;
    }

    public void setNetworkName(String networkName) {
        this.networkName = networkName;
    }

    public String getE164CountryCode() {
        return e164CountryCode;
    }

    public void setE164CountryCode(String e164CountryCode) {
        this.e164CountryCode = e164CountryCode;
    }

    public String getIdp() {
        return idp;
    }

    public void setIdp(String idp) {
        this.idp = idp;
    }

    public boolean isHasCallBarringCapability() {
        return hasCallBarringCapability;
    }

    public void setHasCallBarringCapability(boolean hasCallBarringCapability) {
        this.hasCallBarringCapability = hasCallBarringCapability;
    }

    public boolean isHasUUSDCapability() {
        return hasUUSDCapability;
    }

    public void setHasUUSDCapability(boolean hasUUSDCapability) {
        this.hasUUSDCapability = hasUUSDCapability;
    }

    public boolean isOutboundEnabled() {
        return isOutboundEnabled;
    }

    public void setIsOutboundEnabled(boolean isOutboundEnabled) {
        this.isOutboundEnabled = isOutboundEnabled;
    }

    public int getPermitsPerSecond() {
        return permitsPerSecond;
    }

    public void setPermitsPerSecond(int permitsPerSecond) {
        this.permitsPerSecond = permitsPerSecond;
    }

    public double getBalanceThreshold() {
        return balanceThreshold;
    }

    public void setBalanceThreshold(double balanceThreshold) {
        this.balanceThreshold = balanceThreshold;
    }

    public int getBalanceInterval() {
        return balanceInterval;
    }

    public void setBalanceInterval(int balanceInterval) {
        this.balanceInterval = balanceInterval;
    }

    public List<NetworkDestinationDTO> getNetworkDestinationCodes() {
        return networkDestinationCodes;
    }

    public void setNetworkDestinationCodes(List<NetworkDestinationDTO> networkDestinationCodes) {
        this.networkDestinationCodes = networkDestinationCodes;
    }
}
