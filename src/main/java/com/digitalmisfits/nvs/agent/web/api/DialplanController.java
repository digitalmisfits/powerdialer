package com.digitalmisfits.nvs.agent.web.api;

import com.digitalmisfits.nvs.agent.domain.dto.DialplanDTO;
import com.digitalmisfits.nvs.agent.domain.repository.DialplanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class DialplanController {

    @Autowired
    DialplanRepository dialplanRepository;

    @RequestMapping(value = "/dialplans", method = RequestMethod.GET)
    public List<DialplanDTO> dialplans() {

        List<DialplanDTO> plans = dialplanRepository.findAll()
                .stream()
                .map(dialplan -> {

                    DialplanDTO dialplanDTO = new DialplanDTO();
                    dialplanDTO.setName(dialplan.getName());
                    dialplanDTO.setWeight(dialplan.getWeight());

                    dialplan.getDialplanConnectors().forEach((connector) -> {
                        dialplanDTO.getConnections().add(new DialplanDTO.DialplanNetworkConnection(
                                connector.getOriginationNetworkOperator().getName(),
                                connector.getTerminationNetworkOperator().getName()
                        ));
                    });
                    
                    return dialplanDTO;
                    
                }).collect(Collectors.toList());

        return plans;
    }
}
