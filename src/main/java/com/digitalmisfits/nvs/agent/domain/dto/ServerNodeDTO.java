package com.digitalmisfits.nvs.agent.domain.dto;

public class ServerNodeDTO {

    private Long id;

    private String iso3166Code;

    private String remoteHost;

    private String remoteUser = "openvox";

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIso3166Code() {
        return iso3166Code;
    }

    public void setIso3166Code(String iso3166Code) {
        this.iso3166Code = iso3166Code;
    }

    public String getRemoteHost() {
        return remoteHost;
    }

    public void setRemoteHost(String remoteHost) {
        this.remoteHost = remoteHost;
    }

    public String getRemoteUser() {
        return remoteUser;
    }

    public void setRemoteUser(String remoteUser) {
        this.remoteUser = remoteUser;
    }
}
