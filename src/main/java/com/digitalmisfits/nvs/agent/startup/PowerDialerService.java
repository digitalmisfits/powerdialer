package com.digitalmisfits.nvs.agent.startup;

import com.digitalmisfits.nvs.agent.net.Node;
import com.digitalmisfits.nvs.agent.net.NodeGroup;
import com.digitalmisfits.nvs.agent.service.NodeService;
import com.digitalmisfits.nvs.agent.service.task.ChannelRunnable;
import com.digitalmisfits.nvs.agent.service.task.NodeCallable;
import com.digitalmisfits.nvs.agent.telephony.Channel;
import com.google.common.util.concurrent.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.context.SmartLifecycle;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

@Service
public class PowerDialerService implements SmartLifecycle {

    private static final Logger LOG = Logger.getLogger(PowerDialerService.class.getName());

    @Autowired
    NodeGroup nodeGroup;

    @Autowired
    private NodeService nodeService;

    @Autowired
    private Environment environment;

    /**
     * Node provisioning executor service
     */
    private ListeningExecutorService executor = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());
    
    /**
     * Determines if this service is currently running
     */
    private boolean isRunning = false;

    @Override
    public boolean isAutoStartup() {
        return environment.getProperty("powerdialer.auto-startup", Boolean.class);
    }

    @Override
    public void stop(Runnable callback) {
        stop();
        callback.run();
    }

    @Override
    public void start() {
        LOG.info(String.format("Starting %s", this.getClass().getName()));

        Collection<Node> nodes = nodeService.findAllRegistered();

        if (!nodes.isEmpty()) {
            nodes.forEach((node) -> {
                
                nodeGroup.register(node);
                
                ListenableFuture<Collection<Channel>> channelFuture = executor.submit(getNodeCallable(node));

                Futures.addCallback(channelFuture, new FutureCallback<Collection<Channel>>() {
                    @Override
                    public void onSuccess(Collection<Channel> channels) {

                        node.setState(Node.State.PROVISIONED);
                        
                        channels.forEach((channel) -> {
                            node.getChannelGroup().addChannel(channel, String.format("%s/channel-%d", node.getInetAddress().getHostAddress(), channel.getSpan()), getChannelRunnable(node, channel));
                        });

                        node.getChannelGroup().startAll();
                    }   

                    @Override
                    public void onFailure(Throwable t) {
                        if(t instanceof Channel.OperationException) {
                            node.setState(Node.State.FAILED);
                        }
                        
                        LOG.severe(t.getMessage());
                    }
                });
            });
        }
        
        setRunning(true);
    }

    @Lookup
    public NodeCallable getNodeCallable (Node node) {
        throw new IllegalStateException("Method not instrumented");
    }

    @Lookup
    public ChannelRunnable getChannelRunnable(Node node, Channel channel) {
        throw new IllegalStateException("Method not instrumented");
    }

    @Override
    public void stop() {
        nodeGroup.stopAll();
    }

    @Override
    public boolean isRunning() {
        return isRunning;
    }

    public void setRunning(boolean isRunning) {
        this.isRunning = isRunning;
    }

    @Override
    public int getPhase() {
        return Integer.MAX_VALUE;
    }
}
