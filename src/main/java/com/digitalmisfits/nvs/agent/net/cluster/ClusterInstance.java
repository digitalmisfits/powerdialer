package com.digitalmisfits.nvs.agent.net.cluster;

import com.google.common.base.Preconditions;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Interface to identifiy unique instance in the cluster.
 */
public interface ClusterInstance {

    public static ClusterInstance SYSTEM_PROPERTY = () ->
            Preconditions.checkNotNull(System.getProperty("hurd.instance"));

    public static ClusterInstance SYSTEM_ENVIRONMENT = () ->
            Preconditions.checkNotNull(System.getenv("HURD_INSTANCE"));

    public static ClusterInstance SYSTEM_HOSTNAME = () -> {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    };

    public String getNodeId();
}
