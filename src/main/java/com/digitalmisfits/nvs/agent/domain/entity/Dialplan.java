package com.digitalmisfits.nvs.agent.domain.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "dialplan")
public class Dialplan {

    @Id
    @GeneratedValue
    private Long id;

    /**
     * Dial plan name
     * 
     * e.a. Uganda National (MTN+UTL originate) (call ALL networks using only MTN+UTL as outgoing channel)
     */
    @Column(nullable = false)
    private String name; 

    /**
     * Dialplan weight to determine priority. All priorities are calculated relative to one and other. Can be ANY value.
     * 
     * To be used in SQL conjunction with -LOG(RAND()) / weight
     */
    @Column(nullable = false)
    private int weight = 1;

    /**
     * A dialplan connector creates a connection between an outgoing channel and a target network destination (code)
     */
    @OneToMany(mappedBy="dialplan")
    private List<DialplanConnector> dialplanConnectors;

    /**
     * Date of creation
     */
    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Date createdAt;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public List<DialplanConnector> getDialplanConnectors() {
        return dialplanConnectors;
    }

    public void setDialplanConnectors(List<DialplanConnector> dialplanConnectors) {
        this.dialplanConnectors = dialplanConnectors;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
