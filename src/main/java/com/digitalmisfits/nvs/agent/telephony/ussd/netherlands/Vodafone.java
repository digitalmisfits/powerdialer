package com.digitalmisfits.nvs.agent.telephony.ussd.netherlands;

import com.digitalmisfits.nvs.agent.telephony.ussd.USSDAirtimeConfiguration;
import com.google.common.base.Preconditions;

import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Vodafone USSD Airtime Configuration
 * <p>
 * Note: This class is re-used across channels, when modifying do not retain any state
 */
public final class Vodafone implements USSDAirtimeConfiguration {
    
    private static final Logger LOG = Logger.getLogger(Vodafone.class.getName());

    private static final Pattern BALANCE = Pattern.compile("Je Prepaid tegoed is euro (?<amount>\\d+\\.\\d+)\\.");

    public Vodafone() {
        // implementations need an empty default constructor
    }

    @Override
    public String getISO3166Code() {
        return "NL";
    }

    @Override
    public String getNetworkName() {
        return "vodafone";
    }

    @Override
    public String getBalanceCode() {
        return "*101";
    }

    /**
     * Je Prepaid tegoed is euro 1.78.
     *
     * @param input
     * @return
     */
    @Override
    public double getBalance(String input) {
        Preconditions.checkNotNull(input);

        Matcher m = BALANCE.matcher(input);
        if (m.find()) {
            return Double.valueOf(m.group("amount"));
        } else {
            LOG.severe(String.format("Unable to capture balance from input %s", input));
        }

        return -1;
    }

    @Override
    public String getRechargeCode() {
        return "*102*";
    }

    @Override
    public double getRecharge(String input) {
        throw new UnsupportedOperationException("USSD recharging is not supported on this network");
    }

    @Override
    public String toString() {
        return String.format("%s/%s", getISO3166Code(), getNetworkName());
    }
}
