package com.digitalmisfits.nvs.agent.telephony.ussd.uganda;


import com.digitalmisfits.nvs.agent.telephony.ussd.USSDAirtimeConfiguration;

public class SmartTelecom implements USSDAirtimeConfiguration {

    @Override
    public String getISO3166Code() {
        return "UG";
    }

    @Override
    public String getNetworkName() {
        return "smart";
    }

    @Override
    public String getBalanceCode() {
        return "*130";
    }

    @Override
    public double getBalance(String input) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getRechargeCode() {
        return "*100*";
    }

    @Override
    public double getRecharge(String input) {
        throw new UnsupportedOperationException();
    }
}
