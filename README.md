# Openvox/ Asterisk Powerdialer Prototype

The Openvox/ Asterisk Powerdialer can be used to control a cluster of remotely deployed Openvox/ Asterisk installations to discover active subscribers within a particular telephony network range (ea. 0031647XXXXXX) by pseudo randomly mutating the subscriber extension.

A single node housing up to 4 Openvox GXXX GSM cards can scan approx. 20.000 subscriber extensions per 24 hours depending on network congestion, reliability of the network itself and inter-network connectivity speeds.

Features include:

-
- Call credit monitoring when using pre-paid plans
- Automated call credit top-up strategies using pre-loaded vouchers when using pre-paid plans
- Advanced dialplan configuration to optimize call routing
- Elaborate retry logic in case of failure of either calls or spans
- IMEI rotation to prevent networking blocking
- Can be run fully unattended
- Web interface for configuration and monitoring
