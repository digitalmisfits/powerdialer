package com.digitalmisfits.nvs.agent.service;


import com.digitalmisfits.nvs.agent.telephony.Channel;
import com.google.common.collect.ImmutableSet;

import java.util.concurrent.TimeUnit;

public interface ChannelService {

    ImmutableSet<Channel> queryChannels()
            throws Channel.OperationException;

    Channel.ChannelState queryChannelState(Channel channel)
            throws Channel.OperationException;

    boolean provideChannelState(Channel channel)
            throws Channel.OperationException;

    boolean provision(Channel channel)
            throws Channel.ProvisioningException;

    boolean isProvisioned(Channel channel);

    boolean reset(Channel channel)
            throws Channel.OperationException;

    boolean awaitChannelStateWithReset(Channel channel, final Channel.ChannelState state)
            throws Channel.OperationException;

    boolean awaitChannelState(Channel channel, final Channel.ChannelState state)
            throws Channel.OperationException;

    void interruptChannel(Channel channel)
            throws Channel.OperationException;

    void interruptChannel(Channel channel, long timeout, TimeUnit timeUnit)
            throws Channel.OperationException;

    String awaitUSSDWithReset(Channel channel, String message)
            throws Channel.OperationException;

    String awaitUSSD(Channel channel, String message)
            throws Channel.OperationException;

    String awaitUSSD(Channel channel, String message, long timeout, TimeUnit timeUnit)
            throws Channel.OperationException;

    boolean mutateEquipmentIdentity(Channel channel)
            throws Channel.OperationException;

    Channel.ConnectionState stat(Channel channel, String number)
            throws Channel.OperationException;
}
