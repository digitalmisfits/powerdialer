package com.digitalmisfits.nvs.agent.telephony;


import com.digitalmisfits.nvs.agent.service.task.watcher.ChannelWatcher;
import com.google.common.base.Preconditions;
import com.google.common.collect.EvictingQueue;
import com.google.common.collect.ImmutableSet;
import com.google.common.util.concurrent.RateLimiter;

import java.util.Collection;
import java.util.Date;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Logger;

public class Channel {

    private static final Logger LOG = Logger.getLogger(Channel.class.getName());

    /**
     * Denotes a general channel operation exception
     */
    public static class OperationException extends Exception {

        public OperationException(String message) {
            super(message);
        }

        public OperationException(String message, Throwable cause) {
            super(message, cause);
        }
    }
    
    public static class OperationPatternMatchException extends OperationException {

        public OperationPatternMatchException(String message) {
            super(message);
        }
    }

    /**
     * Denotes that an operation on a channel has been interrupted
     */
    public static class OperationInterruptedException extends OperationException {

        public OperationInterruptedException(String message) {
            super(message);
        }

        public OperationInterruptedException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    /**
     * Denotes a timeout on a channel operation
     */
    public static class TimeoutException extends OperationException {

        public TimeoutException(String message) {
            super(message);
        }

        public TimeoutException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    /**
     * Denotes a channel provisioning exception
     */
    public static class ProvisioningException extends OperationException {

        public ProvisioningException(String message) {
            super(message);
        }

        public ProvisioningException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    /**
     * Denoted an unchecked exception that the channel is not ready yet.
     * Mainly used for retry logic on (temporary) unavailable channels
     */
    public static class ChannelNotReadyException extends RuntimeException {

        public ChannelNotReadyException(String message) {
            super(message);
        }
    }


    public enum ChannelState {

        /**
         * D-Channel is up
         */
        UP("UP"),
        
        /**
         * D-Channel is initializing
         */
        INIT("INIT"),
        
        /**
         * D-Channel is down
         */
        DOWN("DOWN"),
        
        /**
         * D-Channel is ready (and provisioned)
         */
        READY("READY"),
        
        /**
         * D-Channel is preparing to answer an inbound call
         */
        PRE_ANSWER("PRE ANSWER"),
        
        /**
         * D-Channel has inbound call
         */
        CALL_ACTIVE("CALL ACTIVE"),
        
        /**
         * D-Channel is receiving an SMS
         */
        SMS_RECEIVED("SMS RECEIVED"),
        
        /**
         * D-Channel is running an USSD request
         */
        USSD_SENDING("USSD SENDING"),
        
        /**
         * D-Channel is running a phone number check
         */
        PHONE_CHECK("PHONE CHECK"),

        /**
         * D-Channel is running an hangup request
         */
        HANGUP_REQ("HANGUP REQ"),
        
        /**
         * D-Channel is in an other (non-interesting) state
         */
        OTHER("OTHER");

        private final String state;

        ChannelState(String state) {
            this.state = state;
        }

        public static ChannelState defaultValueOf(String state, ChannelState defaultState) {
            for (ChannelState s : ChannelState.values()) {
                if (s.state.equals(state)) {
                    return s;
                }
            }

            return defaultState;
        }
    }

    public enum LinkState {
        /**
         * Link is powered on
         */
        POWER_ON("Power on"),
        /**
         * Link is powered off
         */
        POWER_OFF("Power off"),
        /**
         * Link is provisioned
         */
        PROVISIONED("Provisioned"),
        /**
         * Link is in alarm mode
         */
        IN_ALARM("In Alarm"),
        /**
         * Link is up
         */
        UP("Up"),
        /**
         * Link is down
         */
        DOWN("Down"),
        /**
         * Link has undetected sim card
         */
        NO_SIM("Undetected SIM Card"),
        /**
         * Link has no signal
         */
        NO_SIGNAL("No Signal"),
        /**
         * Link has pin error
         */
        PIN_ERROR("Pin Error"), // original Pin (%s) Error
        /**
         * Link is active
         */
        ACTIVE("Active"),
        /**
         * Link is standby
         */
        STANDBY("Standby"),
        /**
         * Link is running in multiplex mode (virtual tty)
         */
        MULTIPLEXER("Multiplexer"),
        /**
         * Link is not running a virtual tty
         */
        STANDARD("Standard");

        private final String state;

        LinkState(String state) {
            this.state = state;
        }

        public static LinkState defaultValueOf(String state, LinkState defaultState) {
            for (LinkState s : LinkState.values()) {
                if (s.state.equals(state)) {
                    return s;
                }
            }

            return defaultState;
        }
    }

    public enum RegistrationState {
        /*
            Not registered new operator to registered and not searching
         */
        NET_UNREGISTERED("Not registered"),
        /*
           Registered, home network
         */
        HOME("Registered (Home network)"),
        /*
            Not registered, currently searching a new operator to register with
         */
        SEARCHING("Searching"),
        /*
            Registration denied
         */
        DENIED("Registration denied"),
        /*
            Unknown
        */
        UNKNOWN("Unknown"),
        /*
            Registered, roaming
        */
        ROAMING("Registered (Roaming)"),
        /*
            Registered
         */
        REGISTERED("Registered"),
        /*
            Other (Unknown)
         */
        OTHER("Unknown Network Status");

        private final String state;

        RegistrationState(String state) {
            this.state = state;
        }

        public static RegistrationState defaultValueOf(String state, RegistrationState defaultState) {
            for (RegistrationState s : RegistrationState.values()) {
                if (s.state.equals(state)) {
                    return s;
                }
            }

            return defaultState;
        }
    }

    public enum ConnectionState {
        /*
            The current channel (link) is in use
         */
        USING("USING"),
        /*
            The remote party is either connected or busy
         */
        CONNECT("CONNECT"),
        /*
            The remote party is ringing
         */
        RING("RING"),
        /*
            The remote party is busy (e.a. connected to a different party)
         */
        BUSY("BUSY"),
        /*
            The remote party is either busy, powered off or not existing.
         */
        TIMEOUT("TIMEOUT"),
        /*
            The remote party does not exist
         */
        NOEXIST("NOEXIST"),

        /**
         * The connection state is unknown
         */
        UNKNOWN("UNKNOWN");

        private final String state;

        ConnectionState(String state) {
            this.state = state;
        }

        public static ConnectionState defaultValueOf(String state, ConnectionState defaultState) {
            for (ConnectionState s : ConnectionState.values()) {
                if (s.state.equals(state)) {
                    return s;
                }
            }

            return defaultState;
        }
    }

    public enum OperationalState {

        /**
         * The channel is being initialized
         */
        INITIALIZING("INITIALIZING"),

        /**
         * The channel is active from a server point of view
         */
        ACTIVE("ACTIVE"),

        /**
         * The server is (temporary) inactive from a server point of view
         */
        INACTIVE("INACTIVE"),

        /**
         * The channels has no associated operator
         */
        NO_OPERATOR("NO_OPERATOR"),

        /**
         * The channel has no balance or balance top-up failed
         */
        NO_AIRTIME("NO_AIRTIME"),

        /**
         * The channel is temporary unavailable
         */
        TEMP_UNAVAIL("TEMP_UNAVAIL"),

        /**
         * The channel is unusable
         */
        UNUSABLE("UNUSABLE");

        private final String state;

        OperationalState(String state) {
            this.state = state;
        }

        public static OperationalState defaultValueOf(String state, OperationalState defaultState) {
            for (OperationalState s : OperationalState.values()) {
                if (s.state.equals(state)) {
                    return s;
                }
            }

            return defaultState;
        }
    }

    private static final int QUEUE_SIZE = 10;

    private static final int MAINTENANCE_QUEUE_SIZE = 5;
    
    /**
     * The channel watcher thread
     */
    private transient Thread channelWatcher;

    /**
     * The channel inbound queue
     */
    private final transient BlockingQueue<Subscriber> inboundQueue = new LinkedBlockingQueue<>(QUEUE_SIZE);

    /**
     * The channel outbound queue
     */
    private final transient Queue<Subscriber> outboundQueue = new ConcurrentLinkedQueue<>();

    /**
     * The channel maintenance queue
     */
    private final transient BlockingQueue<String> maintenanceQueue = new LinkedBlockingQueue<>(MAINTENANCE_QUEUE_SIZE);

    /**
     * Asterisk span number
     */
    private final int span;

    /**
     * The network operator
     */
    private ChannelOperator channelOperator;

    /**
     * The airtime balance associated with this channelOperator
     */
    private double channelOperatorBalance = -1;

    /**
     * The most recent moment the operator balance was changed
     */
    private Date lastOperatorBalanceChange = new Date();

    /**
     * The channel operational state after provisioning
     */
    private OperationalState operationalState = OperationalState.INITIALIZING;

    /**
     * The most recent moment the operational state was changed
     */
    private Date lastOperationalStateChange = new Date();

    /**
     * Underlying link state
     */
    private Collection<LinkState> linkState = ImmutableSet.of(
            LinkState.POWER_OFF,
            LinkState.DOWN,
            LinkState.ACTIVE,
            LinkState.STANDARD
    );

    /**
     * Signal Quality
     */
    private int signalQuality = -1;

    /**
     * The current underlying gsm state
     */
    private ChannelState channelState = ChannelState.DOWN;

    /**
     * Network registration state
     */
    private RegistrationState registrationState = RegistrationState.UNKNOWN;

    /**
     * Network name
     */
    private String networkName;

    /**
     * The current underlying imei number
     */
    private long imei = 0L;

    /**
     * The send/received last event
     */
    private String lastEvent;

    /**
     * The number of total outbound operatiosn (stat/ sms)
     */
    private AtomicLong billableOperationsCounter = new AtomicLong();

    /**
     * Last seen connection states (from ChannelService.stat())
     */
    private final transient EvictingQueue<ConnectionState> lastConnectionStates = EvictingQueue.create(50);

    /**
     * The channel rate limiter
     */
    private final transient RateLimiter rateLimiter = RateLimiter.create(1.0);

    /**
     * Channel synchronization primitive
     */
    private final transient ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    /**
     * Reentrant Read lock
     */
    public final transient Lock readLock = readWriteLock.readLock();

    /**
     * Reentrant write lock
     */
    public final transient Lock writeLock = readWriteLock.writeLock();

    public Channel(int span) {
        this.span = span;
    }

    /**
     * Start the channel watcher
     *
     * @param watcher the channel watcher
     */
    public void startChannelWatcher(ChannelWatcher watcher) {
        Preconditions.checkNotNull(watcher);

        if (channelWatcher != null && channelWatcher.isAlive()) {
            throw new IllegalStateException("ChannelWatcherThread already running");
        }

        LOG.info(String.format("Starting watcher for channel %d", this.getSpan()));

        // run watcher
        channelWatcher = new Thread(watcher, "Watcher-" + this.getSpan());
        channelWatcher.start();
    }

    /**
     * Stop the channel watcher. Throws InterruptedException when interrupted on join
     *
     * @throws InterruptedException
     */
    public void stopChannelWatcher() throws InterruptedException {

        LOG.info("Stopping channel watcher");

        if (channelWatcher != null) {
            channelWatcher.interrupt();

            try {
                channelWatcher.join(1000 * 10);
            } catch (InterruptedException e) {
                throw e;
            }
        }
    }

    public BlockingQueue<Subscriber> getInboundQueue() {
        return inboundQueue;
    }

    public Queue<Subscriber> getOutboundQueue() {
        return outboundQueue;
    }

    public BlockingQueue<String> getMaintenanceQueue() {
        return maintenanceQueue;
    }

    public int getSpan() {
        return span;
    }

    public ChannelOperator getChannelOperator() {
        return channelOperator;
    }

    public void setChannelOperator(ChannelOperator channelOperator) {
        this.channelOperator = channelOperator;
    }

    public double getChannelOperatorBalance() {
        return channelOperatorBalance;
    }

    public void setChannelOperatorBalance(double channelOperatorBalance) {
        this.channelOperatorBalance = channelOperatorBalance;
    }

    public Date getLastOperatorBalanceChange() {
        return lastOperatorBalanceChange;
    }

    public void setLastOperatorBalanceChange(Date lastOperatorBalanceChange) {
        this.lastOperatorBalanceChange = lastOperatorBalanceChange;
    }

    public OperationalState getOperationalState() {
        return operationalState;
    }

    public void setOperationalState(OperationalState operationalState) {
        this.operationalState = operationalState;

        setLastOperationalStateChange(new Date());
    }

    public Date getLastOperationalStateChange() {
        return lastOperationalStateChange;
    }

    public void setLastOperationalStateChange(Date lastOperationalStateChange) {
        this.lastOperationalStateChange = lastOperationalStateChange;
    }

    public Collection<LinkState> getLinkState() {
        return linkState;
    }

    public void setLinkState(ImmutableSet<LinkState> linkState) {
        this.linkState = linkState;
    }

    public int getSignalQuality() {
        return signalQuality;
    }

    public void setSignalQuality(int signalQuality) {
        this.signalQuality = signalQuality;
    }

    public ChannelState getChannelState() {
        return channelState;
    }

    public void setChannelState(ChannelState channelState) {
        this.channelState = channelState;
    }

    public RegistrationState getRegistrationState() {
        return registrationState;
    }

    public void setRegistrationState(RegistrationState registrationState) {
        this.registrationState = registrationState;
    }

    public String getNetworkName() {
        return networkName;
    }

    public void setNetworkName(String networkName) {
        this.networkName = networkName;
    }

    public long getImei() {
        return imei;
    }

    public void setImei(long imei) {
        this.imei = imei;
    }

    public String getLastEvent() {
        return lastEvent;
    }

    public void setLastEvent(String lastEvent) {
        this.lastEvent = lastEvent;
    }

    public AtomicLong getBillableOperations() {
        return billableOperationsCounter;
    }

    public EvictingQueue<ConnectionState> getLastConnectionStates() {
        return lastConnectionStates;
    }

    public RateLimiter getRateLimiter() {
        return rateLimiter;
    }

    public Lock getReadLock() {
        return readLock;
    }

    public Lock getWriteLock() {
        return writeLock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Channel channel = (Channel) o;

        if (getSpan() != channel.getSpan())
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return getSpan();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Channel{");
        sb.append("channelWatcher=").append(channelWatcher);
        sb.append(", span=").append(span);
        sb.append(", channelOperator=").append(channelOperator);
        sb.append(", linkState=").append(linkState);
        sb.append(", signalQuality=").append(signalQuality);
        sb.append(", channelState=").append(channelState);
        sb.append(", registrationState=").append(registrationState);
        sb.append(", networkName='").append(networkName).append('\'');
        sb.append(", imei=").append(imei);
        sb.append(", lastEvent='").append(lastEvent).append('\'');
        sb.append(", lastConnectionStates=").append(lastConnectionStates);
        sb.append('}');
        return sb.toString();
    }
}

