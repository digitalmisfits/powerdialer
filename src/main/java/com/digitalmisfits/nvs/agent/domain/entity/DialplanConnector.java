package com.digitalmisfits.nvs.agent.domain.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "dpconnector")
public class DialplanConnector implements Serializable {

    /**
     * Determines the network operator for call origination
     */
    @Id
    @ManyToOne
    @JoinColumn(name="originator_id")
    private NetworkOperator originationNetworkOperator;

    /**
     * Determines the network destination code for call termination
     */
    @Id
    @ManyToOne
    @JoinColumn(name="terminator_id")
    private NetworkOperator terminationNetworkOperator;

    /**
     * Many-to-One mapping to the associated dialplan
     */
    @Id
    @ManyToOne
    @JoinColumn(name="dialplan_id")
    private Dialplan dialplan;

    public NetworkOperator getOriginationNetworkOperator() {
        return originationNetworkOperator;
    }

    public void setOriginationNetworkOperator(NetworkOperator originationNetworkOperator) {
        this.originationNetworkOperator = originationNetworkOperator;
    }

    public NetworkOperator getTerminationNetworkOperator() {
        return terminationNetworkOperator;
    }

    public void setTerminationNetworkOperator(NetworkOperator terminationNetworkOperator) {
        this.terminationNetworkOperator = terminationNetworkOperator;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DialplanConnector that = (DialplanConnector) o;

        if (!dialplan.equals(that.dialplan)) return false;
        if (!originationNetworkOperator.equals(that.originationNetworkOperator)) return false;
        if (!terminationNetworkOperator.equals(that.terminationNetworkOperator)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = originationNetworkOperator.hashCode();
        result = 31 * result + terminationNetworkOperator.hashCode();
        result = 31 * result + dialplan.hashCode();
        return result;
    }
}