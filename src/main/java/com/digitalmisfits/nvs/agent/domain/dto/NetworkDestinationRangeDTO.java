package com.digitalmisfits.nvs.agent.domain.dto;

import com.digitalmisfits.nvs.agent.domain.entity.NetworkDestinationRange;


public class NetworkDestinationRangeDTO {

    private Long id;

    private int start;

    private int end;

    private int length;

    private int seed;

    private int registerSize;

    private int transitions;

    private NetworkDestinationRange.RangeState state;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getSeed() {
        return seed;
    }

    public void setSeed(int seed) {
        this.seed = seed;
    }

    public int getRegisterSize() {
        return registerSize;
    }

    public void setRegisterSize(int registerSize) {
        this.registerSize = registerSize;
    }

    public int getTransitions() {
        return transitions;
    }

    public void setTransitions(int transitions) {
        this.transitions = transitions;
    }

    public NetworkDestinationRange.RangeState getState() {
        return state;
    }

    public void setState(NetworkDestinationRange.RangeState state) {
        this.state = state;
    }
}
