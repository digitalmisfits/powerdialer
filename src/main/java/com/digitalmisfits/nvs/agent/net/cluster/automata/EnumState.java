package com.digitalmisfits.nvs.agent.net.cluster.automata;

import java.util.Set;

public interface EnumState<T> {

    public Set<T> possibleFollowUps();

    @Override
    public String toString();
}