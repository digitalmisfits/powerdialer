package com.digitalmisfits.nvs.agent.service.task.watcher;


import com.digitalmisfits.nvs.agent.service.OperatorService;
import com.digitalmisfits.nvs.agent.telephony.Channel;
import com.digitalmisfits.nvs.agent.telephony.Subscriber;
import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.logging.Logger;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class QueueChannelWatcher implements ChannelWatcher {

    private static final Logger LOG = Logger.getLogger(QueueChannelWatcher.class.getName());

    private static final int WAIT_TIME = 5000;

    @Autowired
    private OperatorService provisioningService;

    /**
     * The channel to operate on
     */
    private final Channel channel;


    public QueueChannelWatcher(Channel channel) {
        this.channel = channel;
    }

    public Channel getChannel() {
        return channel;
    }

    @Override
    public void run() {

        Preconditions.checkNotNull(channel, "Channel cannot be null");
        Preconditions.checkNotNull(channel.getChannelOperator(), "ChannelOperator cannot be null");

        Collection<Subscriber> numbers;

        while (!Thread.currentThread().isInterrupted()) {

            try {
                numbers = provisioningService.getNumbers(getChannel().getChannelOperator());

                LOG.info(String.format("%d number(s) received for operator %s on channel %d", numbers.size(), channel.getChannelOperator().getNetworkName(), channel.getSpan()));

                if (!numbers.isEmpty()) {
                    for (Subscriber number : numbers) {
                        getChannel().getInboundQueue().put(number); // blocking put; wait for space to become available;
                    }

                    continue;
                }

                Thread.sleep(WAIT_TIME);
                
            } catch (InterruptedException e) {
                return;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}
