package com.digitalmisfits.nvs.agent.service.impl;

import com.digitalmisfits.nvs.agent.service.AirtimeService;
import com.digitalmisfits.nvs.agent.service.ChannelService;
import com.digitalmisfits.nvs.agent.telephony.Channel;
import com.digitalmisfits.nvs.agent.telephony.ChannelOperator;
import com.digitalmisfits.nvs.agent.telephony.Voucher;
import com.digitalmisfits.nvs.agent.telephony.ussd.USSDAirtimeConfiguration;
import com.digitalmisfits.nvs.agent.telephony.ussd.USSDAirtimeConfigurationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.logging.Logger;

@Service
public class USSDAirtimeService implements AirtimeService {

    private static final Logger LOG = Logger.getLogger(USSDAirtimeService.class.getName());

    @Autowired
    USSDAirtimeConfigurationProvider configurationProvider;

    /**
     * Returns the current airtime balance
     * *
     * @param channelService
     * @param channel the channel
     * @return -1 if not available, > 0 if otherwise
     * @throws Channel.OperationException
     */
    @Override
    public double getBalance(ChannelService channelService, Channel channel)
            throws Channel.OperationException {

        USSDAirtimeConfiguration configuration = lookupUSSDConfiguration(channel.getChannelOperator());
        
        LOG.info(String.format("Retrieving balance for operator %s using configuration %s",
                channel.getChannelOperator().getNetworkName(), configuration.getNetworkName()));

        String response = channelService.awaitUSSD(channel, configuration.getBalanceCode());
        if (response == null) {
            throw new Channel.OperationException("Invalid USSD response");
        }

        LOG.info(String.format("Response for operator %s is %s", channel.getChannelOperator().getNetworkName(), response));

        double balance =  configuration.getBalance(response);
        if(balance == -1)
            throw new Channel.OperationException("Operator balance could not be determined.");
        
        return balance;
    }

    /**
     * Returns the current balance after recharge
     * *
     * @param channelService
     * @param channel
     * @param voucher
     * @return
     * @throws Channel.OperationException
     */
    @Override
    public double recharge(ChannelService channelService, Channel channel, Voucher voucher)
            throws Channel.OperationException {

        USSDAirtimeConfiguration configuration = lookupUSSDConfiguration(channel.getChannelOperator());

        String response = channelService.awaitUSSD(channel, String.format("%s%s#", configuration.getRechargeCode(), voucher.getAccessNumber()));
        if (response == null) {
            throw new Channel.OperationException("Invalid USSD response");
        }

        double balance =  configuration.getRecharge(response);
        if (balance == -1) {
            throw new Channel.OperationException("NetworkOperator post-recharge balance could not be determined.");
        }
        
        return balance;
    }

    private USSDAirtimeConfiguration lookupUSSDConfiguration(ChannelOperator channelOperator) {

        return configurationProvider.lookup(channelOperator.getIso3166CountryCode(), channelOperator.getNetworkName());
    }
}