package com.digitalmisfits.nvs.agent.telephony;


import com.google.common.util.concurrent.RateLimiter;

public class ChannelOperator {

    /**
     * The operator identifier
     */
    private long id;

    /**
     * ISO 3166 country code
     */
    private String iso3166CountryCode;

    /**
     * International Dialing Prefix
     */
    private String idp;

    /**
     * E.164 country code
     */
    private String e164CountryCode;

    /**
     * The operator networkName (.e.a vodafone)
     */
    private String networkName;

    /**
     * Operator specific timeout setting for detecting numbers that are empty, busy or powered off
     */
    private int timeout = 30;

    /**
     * Balance minimum before auto top-up
     */
    private double balanceThreshold = 0.0;

    /**
     * Balance check/top-up interval
     */
    private int balanceInterval = 50;

    /**
     * Global operator rate limiter
     */
    private final transient RateLimiter rateLimiter = RateLimiter.create(1);

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIso3166CountryCode() {
        return iso3166CountryCode;
    }

    public void setIso3166CountryCode(String iso3166CountryCode) {
        this.iso3166CountryCode = iso3166CountryCode;
    }

    public String getIdp() {
        return idp;
    }

    public void setIdp(String idp) {
        this.idp = idp;
    }

    public String getE164CountryCode() {
        return e164CountryCode;
    }

    public void setE164CountryCode(String e164CountryCode) {
        this.e164CountryCode = e164CountryCode;
    }

    public String getNetworkName() {
        return networkName;
    }

    public void setNetworkName(String networkName) {
        this.networkName = networkName;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public double getBalanceThreshold() {
        return balanceThreshold;
    }

    public void setBalanceThreshold(double balanceThreshold) {
        this.balanceThreshold = balanceThreshold;
    }

    public int getBalanceInterval() {
        return balanceInterval;
    }

    public void setBalanceInterval(int balanceInterval) {
        this.balanceInterval = balanceInterval;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChannelOperator operator = (ChannelOperator) o;

        if (id != operator.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ChannelOperator{");
        sb.append("id=").append(id);
        sb.append(", iso3166CountryCode='").append(iso3166CountryCode).append('\'');
        sb.append(", idp='").append(idp).append('\'');
        sb.append(", e164CountryCode='").append(e164CountryCode).append('\'');
        sb.append(", networkName='").append(networkName).append('\'');
        sb.append(", timeout=").append(timeout);
        sb.append('}');
        return sb.toString();
    }
}
