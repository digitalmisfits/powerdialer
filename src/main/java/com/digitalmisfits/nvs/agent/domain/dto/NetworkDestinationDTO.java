package com.digitalmisfits.nvs.agent.domain.dto;

import java.util.Date;


public class NetworkDestinationDTO {

    private Long id;

    private String destinationCode;

    private Date inOperationSince;

    private boolean isActive;
    
    private NetworkDestinationRangeDTO range;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(String destinationCode) {
        this.destinationCode = destinationCode;
    }

    public Date getInOperationSince() {
        return inOperationSince;
    }

    public void setInOperationSince(Date inOperationSince) {
        this.inOperationSince = inOperationSince;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public NetworkDestinationRangeDTO getRange() {
        return range;
    }

    public void setRange(NetworkDestinationRangeDTO range) {
        this.range = range;
    }
}
