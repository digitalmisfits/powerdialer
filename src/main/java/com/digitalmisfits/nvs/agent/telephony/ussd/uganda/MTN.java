package com.digitalmisfits.nvs.agent.telephony.ussd.uganda;

import com.digitalmisfits.nvs.agent.telephony.ussd.USSDAirtimeConfiguration;
import com.google.common.base.Preconditions;

import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MTN implements USSDAirtimeConfiguration {

    private static final Logger LOG = Logger.getLogger(MTN.class.getName());

    private static final Pattern BALANCE;

    static {
        // @formatter:off
        BALANCE     = Pattern.compile("Y'ello. You have (?<balance>[,0-9]+)/-");
        // @formatter:on
    }

    @Override
    public String getISO3166Code() {
        return "UG";
    }

    @Override
    public String getNetworkName() {
        return "MTN-UGANDA";
    }

    @Override
    public String getBalanceCode() {
        return "*156";
    }

    @Override
    public double getBalance(String input) {

        Matcher m = BALANCE.matcher(Preconditions.checkNotNull(input));
        if (m.find()) {
            return Double.valueOf(m.group("balance").replaceAll("[^0-9]", ""));
        } else {
            LOG.severe(String.format("Unable to capture balance from input %s", input));
        }

        return -1;
    }

    @Override
    public String getRechargeCode() {
        return "*155*";
    }

    @Override
    public double getRecharge(String input) {
        throw new UnsupportedOperationException();
    }
}
