package com.digitalmisfits.nvs.agent.web.api;

import com.digitalmisfits.nvs.agent.domain.dto.NodeDTO;
import com.digitalmisfits.nvs.agent.net.NodeGroup;
import com.google.common.collect.ImmutableSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
public class ChannelController {

    @Autowired
    private NodeGroup nodeGroup;

    @RequestMapping(value = "/channels", method = RequestMethod.GET)
    public Collection<NodeDTO> channels() {
        ImmutableSet.Builder<NodeDTO> nodes = ImmutableSet.builder();

        nodeGroup.queryAll().stream().forEach((node) -> {
            nodes.add(new NodeDTO(node, node.getChannelGroup().getAllChannels()));
        });
        
        return nodes.build();
    }
}