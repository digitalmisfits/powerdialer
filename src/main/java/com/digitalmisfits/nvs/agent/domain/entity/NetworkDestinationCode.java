package com.digitalmisfits.nvs.agent.domain.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(
        name = "destinations",
        indexes = {
                @Index(name = "dest_idx", columnList = "destinationCode")
        }
)

public class NetworkDestinationCode {

    @Id
    @GeneratedValue
    private Long id;

    /**
     * Country specific destination code (701, 702, 703, 8001)
     */
    @Column(nullable = false)
    private String destinationCode;

    /**
     * Determines the original activation date of the network destination; nullable
     */
    @Column
    @Temporal(TemporalType.DATE)
    private Date inOperationSince;

    /**
     * Determine if the network destination is available
     */
    @Column(nullable = false)
    private boolean isActive = true;

    /**
     * Many-to-One mapping to the network operator
     */
    @ManyToOne
    @JoinColumn(name = "operator_id")
    private NetworkOperator networkOperator;

    /**
     * One-to-One mapping to the network destination range
     */
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "range_id", referencedColumnName = "id", unique = true)
    private NetworkDestinationRange range;

    public Long getId() {
        return id;
    }

    public String getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(String destinationCode) {
        this.destinationCode = destinationCode;
    }

    public Date getInOperationSince() {
        return inOperationSince;
    }

    public void setInOperationSince(Date inOperationSince) {
        this.inOperationSince = inOperationSince;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public NetworkOperator getNetworkOperator() {
        return networkOperator;
    }

    public void setNetworkOperator(NetworkOperator networkOperator) {
        this.networkOperator = networkOperator;
    }

    public NetworkDestinationRange getRange() {
        return range;
    }

    public void setRange(NetworkDestinationRange range) {
        this.range = range;
    }
}
