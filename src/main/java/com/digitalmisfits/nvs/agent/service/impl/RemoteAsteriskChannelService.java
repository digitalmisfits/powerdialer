package com.digitalmisfits.nvs.agent.service.impl;


import com.digitalmisfits.nvs.agent.net.Node;
import com.digitalmisfits.nvs.agent.service.ChannelService;
import com.digitalmisfits.nvs.agent.support.IMEISupport;
import com.digitalmisfits.nvs.agent.telephony.Channel;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.google.common.io.CharStreams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.backoff.BackOffInterruptedException;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.policy.TimeoutRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Service("RemoteAsterisk")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RemoteAsteriskChannelService implements ChannelService {

    private static final Logger LOG = Logger.getLogger(RemoteAsteriskChannelService.class.getName());

    // AT
    private static final Pattern CREG;
    private static final Pattern CSQ;
    private static final Pattern COPS;

    // STATUS
    private static final Pattern CHANNEL_STATUS;
    private static final Pattern CHANNEL_STATE;
    private static final Pattern CHANNEL_IMEI;
    private static final Pattern NETWORK_NAME;
    private static final Pattern NETWORK_STATUS;

    // GSM
    private static final Pattern SPANS;
    private static final Pattern STAT;
    private static final Pattern STAT_USING;
    private static final Pattern RESET;

    // USSD
    private static final Pattern USSD_RECEIVED;
    private static final Pattern USSD_MESSAGE;

    static {
        // @formatter:off
        CREG            = Pattern.compile("\\+CREG: (?<n>\\d),(?<stat>\\d)");
        CSQ             = Pattern.compile("\\+CSQ: (?<rssi>\\d),(?<ber>\\d)");
        COPS            = Pattern.compile("\\+COPS: (?<mode>\\d),(?<format>\\d),\"(?<oper>[aA-zZ0-9\\s]+)\"");
        SPANS           = Pattern.compile("GSM span (?<span>\\d): (?<states>.*)\\n");
        STAT            = Pattern.compile("PHONE:(#\\d+#)?\\+?\\d+ (?<stat>.*)");
        STAT_USING      = Pattern.compile("SPAN:\\d+ USING");
        RESET           = Pattern.compile("Reset power on span \\d sucess"); // the typo is part of the result
        CHANNEL_STATUS  = Pattern.compile("Status: (?<status>.*)\\n");
        CHANNEL_STATE   = Pattern.compile("State: (?<state>[A-Z\\s]+)( Called (from|to) (.*))?\\n");
        CHANNEL_IMEI    = Pattern.compile("Model IMEI: (?<imei>\\d+)\\n");
        NETWORK_NAME    = Pattern.compile("Network Name: (?<name>.*)\\n");
        NETWORK_STATUS  = Pattern.compile("Network Status: (?<status>.*)\\n");
        USSD_RECEIVED   = Pattern.compile("1:Recive USSD sucess on span \\d"); // the typos are part of the result
        USSD_MESSAGE    = Pattern.compile("USSD Message:(?<message>.*)"); // Match any characters except newline
        // @formatter:on
    }

    private static final ImmutableSet<Channel.LinkState> poweredOnAndUpLinkStates = Sets.immutableEnumSet(
            Channel.LinkState.PROVISIONED,
            Channel.LinkState.POWER_ON,
            Channel.LinkState.UP
    );

    @Autowired
    Environment environment;

    /**
     * The remote host
     */
    private final Node node;

    public RemoteAsteriskChannelService(Node node) {
        this.node = node;
    }

    /**
     * Query Asterisk in order do discover all gsm spans
     *
     * @return an immutable list of spans
     * @throws Channel.OperationException
     */
    @Override
    public ImmutableSet<Channel> queryChannels() throws Channel.OperationException {

        ImmutableSet.Builder channels = new ImmutableSet.Builder();

        StringBuffer result = new StringBuffer();

        if (executeOnCli(null, "gsm show spans", result) == 0) { // blocking

            Matcher matcher = SPANS.matcher(result);
            while (matcher.find()) {
                channels.add(new Channel(Integer.valueOf(matcher.group("span"))));
            }

            return channels.build();
        }

        throw new Channel.OperationException("Channel state could not be determined");
    }

    /**
     * D-channel: 2<br/>
     * Status: Power on, Provisioned, Up, Active, Standard<br/>
     * Type: CPE<br/>
     * Manufacturer: Revision: MTK 0828<br/>
     * Model Name: Quectel_M20<br/>
     * Model IMEI: 861111003386772<br/>
     * Revision:  M20R06A03N32<br/>
     * Network Name: vodafone<br/>
     * Network Status: Registered (Home network)<br/>
     * Signal Quality (0,31): 17<br/>
     * SIM IMSI: 204045570238691<br/>
     * SIM SMS Center Number: +316540881000<br/>
     * Last event: Hangup<br/>
     * State: READY<br/> <---------- this is searched
     * Last send AT: ATH\r\n<br/>
     * Last receive AT: \r\nOK\r\n<br/>
     */
    @Override
    public Channel.ChannelState queryChannelState(Channel channel) throws Channel.OperationException {

        LOG.entering(RemoteAsteriskChannelService.class.getName(), "queryChannelState");

        StringBuffer result = new StringBuffer();

        if (executeOnCli(channel, String.format("gsm show span %d", channel.getSpan()), result) == 0) { // blocking

            Matcher matcher = CHANNEL_STATE.matcher(result);
            if (matcher.find()) {
                return Channel.ChannelState.defaultValueOf(matcher.group("state"), Channel.ChannelState.OTHER);
            }
        }

        throw new Channel.OperationException("Channel state could not be determined");
    }

    /**
     * Provide a channel with all the various states. see queryChannelState
     *
     * @param channel the channel to provision
     * @return true if succes, false if otherwise
     * @throws Channel.OperationException
     */
    @Override
    public boolean provideChannelState(Channel channel) throws Channel.OperationException {

        LOG.info(String.format("Providing ChannelState on channel %d", channel.getSpan()));

        StringBuffer result = new StringBuffer();

        if (executeOnCli(channel, String.format("gsm show span %d", channel.getSpan()), result) == 0) { // blocking

            // retrieve channel status
            Matcher channelStatusMatcher = CHANNEL_STATUS.matcher(result);
            if (channelStatusMatcher.find()) {
                channel.setLinkState(parseChannelLinkStates(channelStatusMatcher.group("status")));
            } else {
                LOG.severe(String.format("Could not determine LinkState on channel %d for %s", channel.getSpan(), result));
            }

            // retrieve channel state
            Matcher channelStateMatcher = CHANNEL_STATE.matcher(result);
            if (channelStateMatcher.find()) {
                channel.setChannelState(Channel.ChannelState.defaultValueOf(channelStateMatcher.group("state"), Channel.ChannelState.OTHER));
            } else {
                LOG.severe(String.format("Could not determine ChannelState on channel %d for %s", channel.getSpan(), result));
            }

            // retrieve channel imei number
            Matcher channelImeiMatcher = CHANNEL_IMEI.matcher(result);
            if (channelImeiMatcher.find()) {
                channel.setImei(Long.valueOf(channelImeiMatcher.group("imei")));
            }

            // retrieve network name
            Matcher networkNameMatcher = NETWORK_NAME.matcher(result);
            if (networkNameMatcher.find()) {
                channel.setNetworkName(networkNameMatcher.group("name"));
            } else {
                LOG.severe(String.format("Could not determine NetworkName on channel %d for %s", channel.getSpan(), result));
            }

            // retrieve network registration status
            Matcher networkStateMatcher = NETWORK_STATUS.matcher(result);
            if (networkStateMatcher.find()) {
                channel.setRegistrationState(Channel.RegistrationState.defaultValueOf(networkStateMatcher.group("status"), Channel.RegistrationState.OTHER));
            } else {
                LOG.severe(String.format("Could not determine RegistrationState on channel %d for %s", channel.getSpan(), result));
            }

            return true;
        }

        throw new Channel.OperationException(String.format("Channel state could not be determined for [%s]", result));
    }

    /**
     * Transforms a comma separated list of LinkStates into an ImmutableSet of ListState enums
     *
     * @param states comma separated list of states e.a. Power on, Provisioned, Up, Active, Standard, etc.
     * @return
     */
    private ImmutableSet<Channel.LinkState> parseChannelLinkStates(String states) {
        Preconditions.checkNotNull(states);

        Iterable<String> s = Splitter.on(',').trimResults().omitEmptyStrings().split(states);

        return Sets.immutableEnumSet(Iterables.transform(s, (String input) -> {
            try {
                if (input.toLowerCase().startsWith("pin")) {
                    return Channel.LinkState.PIN_ERROR; // pin error state is Pin (%s) Error
                }
                return Channel.LinkState.defaultValueOf(input, null);
            } catch (IllegalArgumentException e) {
                return null;
            }
        }));
    }

    @Override
    public boolean provision(Channel channel) throws Channel.ProvisioningException {

        try {
            provideChannelState(channel);
        } catch (Channel.OperationException e) {
            throw new Channel.ProvisioningException("Channel could not be fully provisioned", e);
        }

        return isProvisioned(channel);
    }

    @Override
    public boolean isProvisioned(Channel channel) {

        try {
            Preconditions.checkNotNull(channel.getNetworkName());
            Preconditions.checkNotNull(channel.getLinkState());
            Preconditions.checkNotNull(channel.getChannelState());
            Preconditions.checkNotNull(channel.getRegistrationState());
        } catch (NullPointerException e) {
            return false;
        }

        return true;
    }

    @Override
    public boolean awaitChannelStateWithReset(Channel channel, Channel.ChannelState state) throws Channel.OperationException {

        final ExponentialBackOffPolicy backOffPolicy = new ExponentialBackOffPolicy();
        backOffPolicy.setMultiplier(2);
        backOffPolicy.setMaxInterval(TimeUnit.MINUTES.toMillis(5)); // 5 minutes
        backOffPolicy.setInitialInterval(3000); // 3 seconds

        final TimeoutRetryPolicy retryPolicy = new TimeoutRetryPolicy();
        retryPolicy.setTimeout(TimeUnit.HOURS.toMillis(72));

        final RetryTemplate retry = new RetryTemplate();
        retry.setRetryPolicy(retryPolicy);
        retry.setBackOffPolicy(backOffPolicy);

        try {
            retry.execute(context -> {
                if (!awaitChannelState(channel, state)) {
                    LOG.info(String.format("Channel %d did not became ready in a timely fashion, running power cycle", channel.getSpan()));

                    if (!reset(channel)) {
                        throw new Channel.OperationException("The channel could not be fully provisioned. ");
                    }
                }
                return true;
            });
        } catch (BackOffInterruptedException e) {
            throw new Channel.OperationInterruptedException("Retry BackOff interrupted", e);
        }

        return false;
    }

    /**
     * Await for a channel to have successfully transitioned into a give state
     * while requiring Provisioned, Up and Active channel LinkState
     *
     * @param channel the channel
     * @param state   the state
     * @return true if succeeded, false if otherwise
     */
    @Override
    public boolean awaitChannelState(Channel channel, final Channel.ChannelState state)
            throws Channel.OperationException {

        LOG.info(String.format("Awaiting ChannelState %s on channel %d", state, channel.getSpan()));

        final ExponentialBackOffPolicy backOffPolicy = new ExponentialBackOffPolicy();
        backOffPolicy.setMultiplier(2);
        backOffPolicy.setMaxInterval(TimeUnit.MINUTES.toMillis(5));
        backOffPolicy.setInitialInterval(1000);

        final TimeoutRetryPolicy retryPolicy = new TimeoutRetryPolicy();
        retryPolicy.setTimeout(TimeUnit.MINUTES.toMillis(30));

        final RetryTemplate retry = new RetryTemplate();
        retry.setRetryPolicy(retryPolicy);
        retry.setBackOffPolicy(backOffPolicy);

        Channel.ChannelState result = null;
        try {
            result = retry.execute(new RetryCallback<Channel.ChannelState, Channel.OperationException>() {
                @Override
                public Channel.ChannelState doWithRetry(RetryContext context) throws Channel.OperationException {

                    LOG.info(String.format("Requesting ChannelState %s on channel %d with retry count %d", state, channel.getSpan(), context.getRetryCount()));

                    try {
                        provideChannelState(channel);

                        if (channel.getChannelState() == state && channel.getLinkState().containsAll(poweredOnAndUpLinkStates)) {
                            return state;
                        }

                        LOG.info(String.format("ChannelState %s and LinkState %s on channel %d does not match requested ChannelState %s and LinkState %s. Retrying",
                                channel.getChannelState(), channel.getLinkState(), channel.getSpan(), state, poweredOnAndUpLinkStates
                        ));
                    } catch (Channel.OperationException e) {
                        // ignore
                    }

                    throw new Channel.ChannelNotReadyException("Channel did not transition into the requested state in a timely fashion");
                }

            }, (context) -> null);

        } catch (BackOffInterruptedException e) {
            throw new Channel.OperationInterruptedException("Retry BackOff interrupted", e);
        }

        return result == state;
    }

    /**
     * This implementation runs a power cycle on the span associated with this channel.
     * 
     * <p>
     * gsm power reset <span>  -> Reset power on span 1 sucess
     *
     * @param channel The channel to run the power cycle on
     * @return true if successful, false otherwise
     * @throws Channel.OperationException
     */
    @Override
    public boolean reset(Channel channel) throws Channel.OperationException {

        LOG.info(String.format("Running power cycle on channel %d", channel.getSpan()));

        StringBuffer result = new StringBuffer();

        if (executeOnCli(channel, String.format("gsm power reset %d", channel.getSpan()), result) == 0) { // blocking

            Matcher matcher = RESET.matcher(result);
            if (matcher.find()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Verify phone number
     *
     * @param channel The outbound channel
     * @param number  The number to stat
     */
    @Override
    public Channel.ConnectionState stat(Channel channel, String number)
            throws Channel.OperationException {

        channel.getBillableOperations().incrementAndGet();

        StringBuffer result = new StringBuffer();
        if (executeOnCli(channel, String.format("gsm check phone stat %d %s 1", channel.getSpan(), number), result) == 0) {

            Matcher matcher = STAT.matcher(result);
            if (matcher.find()) {
                return Channel.ConnectionState.defaultValueOf(matcher.group("stat").toUpperCase(), Channel.ConnectionState.NOEXIST);
            } else {
                Matcher using = STAT_USING.matcher(result);
                if (using.find())
                    return Channel.ConnectionState.USING;
                else {
                    LOG.severe(String.format("Unable to determine ConnectionState on channel %d for %s", channel.getSpan(), result));
                }
            }
        }

        throw new Channel.OperationException("Connection state could no be determined.");
    }

    /**
     * ATH / AT+CHUP
     *
     * @param channel The channel to interrupt
     */
    @Override
    public void interruptChannel(Channel channel)
            throws Channel.OperationException {

        interruptChannel(channel, 5, TimeUnit.SECONDS);
    }

    /**
     * ATH / AT+CHUP
     *
     * @param channel  The channel to interrupt
     * @param timeout
     * @param timeUnit
     */
    @Override
    public void interruptChannel(Channel channel, long timeout, TimeUnit timeUnit)
            throws Channel.OperationException {

        StringBuffer result = new StringBuffer();

        try {
            if (executeAT(channel, "ATH", result) == 0) {
                try {
                    timeUnit.sleep(timeout);
                } catch (InterruptedException e) {
                    return;
                }
            }
        } catch (Channel.TimeoutException e) {
            return;
        } catch (Channel.OperationException e) {
            // This is serious business; inform someone
        }
    }

    /**
     * Changes the IMEI# for the channel. After the IMEI has been set, a power cycle must be completed
     * in order for changes to take effect
     * <p>
     * gsm send at 1 AT+EGMR=1,7,\"XXXXXX\"
     * <p>
     * EXPERIMENTAL: AT commands are not being piped back into the remote unix connection therefor its not possible
     * to check the result
     *
     * @param channel The channel
     */
    @Override
    public boolean mutateEquipmentIdentity(Channel channel) throws Channel.OperationException {

        StringBuffer result = new StringBuffer();

        if (executeAT(channel, String.format("AT+EGMR=1,7,\\\"%d\\\"", IMEISupport.mutate(channel.getImei())), result) == 0) {
            return reset(channel);
        }

        return false;
    }

    @Override
    public String awaitUSSDWithReset(Channel channel, String message)
            throws Channel.OperationException {

        final ExponentialBackOffPolicy backOffPolicy = new ExponentialBackOffPolicy();
        backOffPolicy.setMultiplier(2);
        backOffPolicy.setMaxInterval(TimeUnit.MINUTES.toMillis(5));
        backOffPolicy.setInitialInterval(3000);

        final SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy();
        retryPolicy.setMaxAttempts(5);

        final RetryTemplate retry = new RetryTemplate();
        retry.setRetryPolicy(retryPolicy);
        retry.setBackOffPolicy(backOffPolicy);

        try {
            return retry.execute(context -> {
                try {
                    return awaitUSSD(channel, message);
                } catch (Channel.OperationException e) {
                    try {
                        reset(channel);
                        awaitChannelState(channel, Channel.ChannelState.READY);
                    } catch (Channel.OperationException e1) {
                        // ignore
                    }

                    throw e;
                }
            });
        } catch (BackOffInterruptedException e) {
            throw new Channel.OperationInterruptedException("Retry BackOff interrupted", e);
        }
    }

    /**
     * CLI> gsm send ussd 1 *101#
     * 1:Recive USSD sucess on span 1
     * USSD Responses:2
     * USSD Code:0
     * USSD Message:Je Prepaid tegoed is euro 1.78. Geldig tot 18/08/2015. Nooit zonder tegoed? Kijk op vodafone.nl/opwaarderen voor Overal Opwaarderen.
     *
     * @param channel
     * @param message
     * @todo Add reset(channel)
     */
    @Override
    public String awaitUSSD(Channel channel, String message)
            throws Channel.OperationException {

        final ExponentialBackOffPolicy backOffPolicy = new ExponentialBackOffPolicy();
        backOffPolicy.setMultiplier(2);
        backOffPolicy.setMaxInterval(TimeUnit.MINUTES.toMillis(5));
        backOffPolicy.setInitialInterval(3000);

        final SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy();
        retryPolicy.setMaxAttempts(5);

        final RetryTemplate retry = new RetryTemplate();
        retry.setRetryPolicy(retryPolicy);
        retry.setBackOffPolicy(backOffPolicy);

        try {
            return retry.execute(context -> {
                try {
                    return awaitUSSD(channel, message, 60, TimeUnit.SECONDS);
                } catch (Channel.OperationException e) {
                    LOG.info(String.format("USSD failed with message %s. Retrying with retry count %d", e.getMessage(), context.getRetryCount()));
                    throw e; // optionally set channel state
                }
            });
        } catch (BackOffInterruptedException e) {
            throw new Channel.OperationInterruptedException("Retry BackOff interrupted", e);
        }
    }

    @Override
    public String awaitUSSD(Channel channel, String message, long timeout, TimeUnit timeUnit)
            throws Channel.OperationException {

        StringBuffer result = new StringBuffer();
        if (executeOnCli(channel, String.format("gsm send ussd %d %s#", channel.getSpan(), message), result) == 0) {

            Matcher messageMatcher = USSD_MESSAGE.matcher(result);
            if (USSD_RECEIVED.matcher(result).find() && messageMatcher.find()) {
                return messageMatcher.group("message");
            }

            throw new Channel.OperationPatternMatchException(
                    String.format("USSD pattern matching failed on channel %d for result %s", channel.getSpan(), result));
        }

        throw new Channel.OperationException("USSD operation could not be completed. ");
    }

    /**
     * Execute an AT Command
     *
     * @param channel
     * @param command
     * @param sink
     * @return
     * @throws Channel.OperationException
     */
    protected int executeAT(Channel channel, String command, Appendable sink)
            throws Channel.OperationException {

        return executeAT(channel, command, sink, 30, TimeUnit.SECONDS);
    }

    /**
     * Execute an AT command
     *
     * @param channel
     * @param command
     * @param sink
     * @param timeout
     * @param timeUnit
     * @return
     * @throws Channel.OperationException
     */
    protected int executeAT(Channel channel, String command, Appendable sink, long timeout, TimeUnit timeUnit)
            throws Channel.OperationException {

        return executeOnCli(String.format("gsm send at %d %s", channel.getSpan(), command), sink, timeout, timeUnit);
    }

    protected int executeOnCli(Channel channel, String argument, Appendable sink)
            throws Channel.OperationException {

        final ExponentialBackOffPolicy backOffPolicy = new ExponentialBackOffPolicy();
        backOffPolicy.setMultiplier(2);
        backOffPolicy.setMaxInterval(TimeUnit.MINUTES.toMillis(5));
        backOffPolicy.setInitialInterval(3000);

        final TimeoutRetryPolicy retryPolicy = new TimeoutRetryPolicy();
        retryPolicy.setTimeout(TimeUnit.HOURS.toMillis(48));

        final RetryTemplate retry = new RetryTemplate();
        retry.setRetryPolicy(retryPolicy);
        retry.setBackOffPolicy(backOffPolicy);

        try {
            return retry.execute(context -> {
                try {
                    return executeOnCli(argument, sink, 60, TimeUnit.SECONDS);
                } catch (Channel.OperationException e) {
                    LOG.info(String.format("CLI execution failed with message %s. Retrying with retry count %d", e.getMessage(), context.getRetryCount()));
                    throw e; // optionally set channel state
                }
            });
        } catch (BackOffInterruptedException e) {
            throw new Channel.OperationInterruptedException("Retry BackOff interrupted", e);
        }
    }

    /**
     * ssh -t openvox@192.168.0.106 'sudo /usr/sbin/system.asterisk -rx "gsm show span 1"'
     *
     * @param argument
     * @param sink
     * @param timeout
     * @param timeUnit
     * @return
     * @throws Channel.OperationException
     */
    protected int executeOnCli(String argument, Appendable sink, long timeout, TimeUnit timeUnit)
            throws Channel.OperationException {

        ProcessBuilder builder = new ProcessBuilder();
        builder.redirectErrorStream(true);
        builder.command(
                "/usr/bin/ssh",
                String.format("openvox@%s", getInetAddress().getHostAddress()),
                String.format("sudo /usr/sbin/asterisk -rx '%s'", argument));

        LOG.info(String.format("Executing shell command %s", builder.command().toString()));

        try {
            Process process = builder.start();
            try {
                if (!process.waitFor(timeout, timeUnit)) {
                    process.destroyForcibly();
                    throw new Channel.TimeoutException("Remote asterisk operation timed out.");
                }

                CharStreams.copy(new InputStreamReader(process.getInputStream()), sink);

                if (process.exitValue() > 0) {
                    throw new Channel.OperationException(String.format("SSH exit code == %d, response %s", process.exitValue(), sink));
                }

                return process.exitValue();

            } catch (InterruptedException e) {
                if (process.isAlive()) {
                    process.destroyForcibly();

                    // re-interrupt current thread
                    Thread.currentThread().interrupt();
                }
            }
        } catch (IOException e) {
            LOG.info(e.getMessage());
            throw new Channel.OperationException("Asterisk operation failed.", e);
        }

        return -1;
    }

    public InetAddress getInetAddress() {
        return node.getInetAddress();
    }
}
