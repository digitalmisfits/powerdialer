package com.digitalmisfits.nvs.agent.net;

import com.digitalmisfits.nvs.agent.telephony.ChannelGroup;

import java.net.InetAddress;

public class Node implements Cloneable {

    /**
     * Denotes a general channel Node exception
     */
    public static class OperationException extends Exception {

        public OperationException(String message) {
            super(message);
        }

        public OperationException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    public enum State {
        LATENT,
        INITIALIZING,
        PROVISIONED,
        SUSPENDED,
        STOPPED,
        FAILED
    }

    /**
     * The node identifier
     */
    private final long nodeId;

    /**
     * The node endpoint
     */
    private final InetAddress inetAddress;

    /**
     * iso3166 country code associated with this node
     */
    private final String iso3166CountryCode;

    /**
     * The internal node state
     */
    private State state = State.LATENT;

    /**
     * The ChannelGroup associated with this node
     */
    private final transient ChannelGroup channelGroup = new ChannelGroup();

    /**
     * @param nodeId      The internal node identifier
     * @param iso3166CountryCode The iso 3166 country code
     * @param inetAddress The remote host
     */
    public Node(long nodeId, String iso3166CountryCode, InetAddress inetAddress) {
        this.nodeId = nodeId;
        this.iso3166CountryCode = iso3166CountryCode;
        this.inetAddress = inetAddress;
    }

    public long getNodeId() {
        return nodeId;
    }

    public InetAddress getInetAddress() {
        return inetAddress;
    }

    public String getIso3166CountryCode() {
        return iso3166CountryCode;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public ChannelGroup getChannelGroup() {
        return channelGroup;
    }
}
