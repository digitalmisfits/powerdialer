package com.digitalmisfits.nvs.agent.net;

import com.google.common.collect.ImmutableSet;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

@Component
public class NodeGroup {

    private static final Logger LOG = Logger.getLogger(NodeGroup.class.getName());

    /**
     * Repository containing all the available nodes
     */
    private final Map<Long, Node> nodes = new ConcurrentHashMap<>();

    /**
     * Add a node to the node group
     *
     * @param node
     */
    public Node register(Node node) {
        return nodes.put(node.getNodeId(), node);
    }

    /**
     * Remove a node from the node group
     *
     * @param nodeId
     */
    public void unregister(long nodeId) {
        throw new UnsupportedOperationException();
    }

    /**
     * Return all nodes
     *
     * @return
     */
    public Collection<Node> queryAll() {
        return ImmutableSet.copyOf(nodes.values());
    }

    /**
     * Return a single node based on the id
     *
     * @param id the id
     * @return the node
     */
    public Node query(long id) {
        return nodes.get(id);
    }

    /**
     * Stop all the nodes in the node group
     */
    public synchronized void stopAll() {

        LOG.info("Stopping " + this.getClass().getName());

        nodes.forEach((nodeId, node) -> {
            node.getChannelGroup().stop();
        });
    }
}
