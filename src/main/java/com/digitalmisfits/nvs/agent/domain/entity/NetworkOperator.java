package com.digitalmisfits.nvs.agent.domain.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(
        name = "operators",
        indexes = {
                @Index(name = "oper_idx", columnList = "iso3166,networkName", unique = true)
        })
public class NetworkOperator {

    public static enum NetworkState {
        OPERATIONAL, // network is operational
        SUSPENDED, // network is (temporary) suspended
        IGNORED // network is fully ignored
    }

    @Id
    @GeneratedValue
    private Long id;

    /**
     * ISO3166 Country code
     */
    @Column(name = "iso3166", nullable = false)
    private String iso3166CountryCode; // UG

    /**
     * Full network name
     */
    @Column(nullable = false)
    private String name; // Uganda Telecom Limited

    /**
     * Sim network name
     */
    @Column(nullable = false)
    private String networkName; // usually HNI

    /**
     * The combination of MCC and MNC is called HNI (Home network identity)
     * and is the combination of both in one string (e.g. MCC= 262 and MNC = 01 results in an HNI of 26201).
     */
    @Column(nullable = true)
    private String homeNetworkIdentity;

    /**
     * Mobile Country Code (MCC)
     */
    @Column(nullable = true)
    private String mobileCountryCode;

    /**
     * Mobile Network Code (MNC)
     */
    @Column(nullable = true)
    private String mobileNetworkCode;

    /**
     * E.164 telecom country prefix
     */
    @Column(name = "e164", nullable = false)
    private String e164CountryCode; // (+)256

    /**
     * International Dialing Prefix (defaults to 00)
     */
    @Column(nullable = false)
    private String idp = "00";

    /**
     * Determines whether this operator allows call barring
     */
    @Column(name = "callbarring", nullable = false)
    private boolean hasCallBarringCapability = true; // default operator allows call barring

    /**
     * Determines whether this operator allows USSD
     */
    @Column(name = "ussd", nullable = false)
    private boolean hasUUSDCapability = true; // default operator allows USSD

    /**
     * Determines if this channel is outbound (e.a. can originate calls) enabled
     */
    @Column(name = "outbound", nullable = false)
    private boolean isOutboundEnabled;

    /**
     * Local number of permits (outgoing calls) per second (effective rate limiter)
     */
    @Column(name = "pps", nullable = false)
    private int permitsPerSecond = 1; // default is 1 outgoing call per second

    /**
     * Network state
     */
    @Column(nullable = false)
    //@Enumerated(EnumType.STRING)
    private NetworkState state = NetworkState.OPERATIONAL;

    /**
     * State transition log
     */
    @Column
    @Basic(fetch = FetchType.LAZY)
    @ElementCollection
    private List<String> log;

    /**
     * Balance minimum before auto top-up
     */
    @Column(nullable = false)
    private double balanceThreshold = 0.0;

    /**
     * Balance check/top-up interval
     */
    @Column(nullable = false)
    private int balanceInterval = 50;

    /**
     * Attached list of network destination codes
     */
    @OneToMany(mappedBy = "networkOperator", fetch = FetchType.LAZY)
    private List<NetworkDestinationCode> destinationCodes; // 710, 711, 712, etc.

    /**
     * Attached list of (available) airtime vouchers for automated top-up
     */
    @OneToMany(mappedBy = "networkOperator", fetch = FetchType.LAZY)
    private List<AirtimeVoucher> airtimeVouchers;


    public Long getId() {
        return id;
    }

    public String getIso3166CountryCode() {
        return iso3166CountryCode;
    }

    public void setIso3166CountryCode(String iso3166CountryCode) {
        this.iso3166CountryCode = iso3166CountryCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNetworkName() {
        return networkName;
    }

    public void setNetworkName(String networkName) {
        this.networkName = networkName;
    }

    public String getHomeNetworkIdentity() {
        return homeNetworkIdentity;
    }

    public void setHomeNetworkIdentity(String homeNetworkIdentity) {
        this.homeNetworkIdentity = homeNetworkIdentity;
    }

    public String getMobileCountryCode() {
        return mobileCountryCode;
    }

    public void setMobileCountryCode(String mobileCountryCode) {
        this.mobileCountryCode = mobileCountryCode;
    }

    public String getMobileNetworkCode() {
        return mobileNetworkCode;
    }

    public void setMobileNetworkCode(String mobileNetworkCode) {
        this.mobileNetworkCode = mobileNetworkCode;
    }

    public String getE164CountryCode() {
        return e164CountryCode;
    }

    public void setE164CountryCode(String e164CountryCode) {
        this.e164CountryCode = e164CountryCode;
    }

    public String getIdp() {
        return idp;
    }

    public void setIdp(String idp) {
        this.idp = idp;
    }

    public boolean isHasCallBarringCapability() {
        return hasCallBarringCapability;
    }

    public void setHasCallBarringCapability(boolean hasCallBarringCapability) {
        this.hasCallBarringCapability = hasCallBarringCapability;
    }

    public boolean isHasUUSDCapability() {
        return hasUUSDCapability;
    }

    public void setHasUUSDCapability(boolean hasUUSDCapability) {
        this.hasUUSDCapability = hasUUSDCapability;
    }

    public boolean isOutboundEnabled() {
        return isOutboundEnabled;
    }

    public void setOutboundEnabled(boolean isOutboundEnabled) {
        this.isOutboundEnabled = isOutboundEnabled;
    }

    public int getPermitsPerSecond() {
        return permitsPerSecond;
    }

    public void setPermitsPerSecond(int permitsPerSecond) {
        this.permitsPerSecond = permitsPerSecond;
    }

    public NetworkState getState() {
        return state;
    }

    public void setState(NetworkState state) {
        this.state = state;
    }

    public List<String> getLog() {
        return log;
    }

    public void setLog(List<String> log) {
        this.log = log;
    }

    public double getBalanceThreshold() {
        return balanceThreshold;
    }

    public void setBalanceThreshold(double balanceThreshold) {
        this.balanceThreshold = balanceThreshold;
    }

    public int getBalanceInterval() {
        return balanceInterval;
    }

    public void setBalanceInterval(int balanceInterval) {
        this.balanceInterval = balanceInterval;
    }

    public List<NetworkDestinationCode> getDestinationCodes() {
        return destinationCodes;
    }

    public void setDestinationCodes(List<NetworkDestinationCode> destinationCodes) {
        this.destinationCodes = destinationCodes;
    }

    public List<AirtimeVoucher> getAirtimeVouchers() {
        return airtimeVouchers;
    }

    public void setAirtimeVouchers(List<AirtimeVoucher> airtimeVouchers) {
        this.airtimeVouchers = airtimeVouchers;
    }
}
