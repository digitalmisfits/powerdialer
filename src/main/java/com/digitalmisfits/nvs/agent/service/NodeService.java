package com.digitalmisfits.nvs.agent.service;


import com.digitalmisfits.nvs.agent.net.Node;

import java.util.Collection;

public interface NodeService {
    
    public Collection<Node> findAllRegistered();
}
