package com.digitalmisfits.nvs.agent.domain.dto;

import com.digitalmisfits.nvs.agent.net.Node;
import com.digitalmisfits.nvs.agent.telephony.Channel;

import java.util.Collection;

public class NodeDTO {

    private Node node;

    private Collection<Channel> channels;

    public NodeDTO(Node node, Collection<Channel> channels) {
        this.node = node;
        this.channels = channels;
    }

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public Collection<Channel> getChannels() {
        return channels;
    }

    public void setChannels(Collection<Channel> channels) {
        this.channels = channels;
    }
}
