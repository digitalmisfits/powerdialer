package com.digitalmisfits.nvs.agent.telephony;

import com.digitalmisfits.nvs.agent.service.task.ChannelRunnable;
import com.google.common.collect.ImmutableSet;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

@Component
public class ChannelGroup {

    private static final Logger LOG = Logger.getLogger(ChannelGroup.class.getName());

    /**
     * Channel/Thread map container
     */
    private final transient Map<Channel, Thread> threads = new ConcurrentHashMap<>();

    /**
     * Indicate this ChannelGroup is started
     */
    private boolean isStarted = false;

    /**
     * Add a channel and associated thread to the channel group
     *
     * @param channel  The channel to register
     * @param runnable The associated channel runnable
     */
    public void addChannel(Channel channel, String identifier, ChannelRunnable runnable) {
        threads.put(channel, new Thread(runnable, identifier));
    }

    /**
     * Remove a channel (and associated Thread) from the channel group
     *
     * @param channel The channel to unregister
     * @return
     */
    public Thread removeChannel(Channel channel) {
        return threads.remove(channel);
    }

    /**
     * Retrieve an associated Thread from the channel
     *
     * @param channel The channel to lookup the thread for
     * @return The associated channel Thread
     */
    public Thread getChannelThread(Channel channel) {
        return threads.get(channel);
    }

    /**
     * Return all channel threads
     * *
     * @return
     */
    public Collection<Thread> getAllChannelThreads() {
        return ImmutableSet.copyOf(threads.values());
    }

    /**
     * Returns an immutable set of all channels
     *
     * @return the channels
     */
    public Collection<Channel> getAllChannels() {
        return ImmutableSet.copyOf(threads.keySet());
    }
    
    /**
     * Start all channel threads in this ChannelGroup
     */
    public synchronized void startAll() {

        if (isStarted()) {
            throw new IllegalStateException("ChannelGroup already started");
        }

        threads.forEach((channel, thread) -> {
            LOG.info(String.format("Starting ChannelGroup thread %s", thread.getName()));

            if (thread.getState() == Thread.State.NEW) {
                thread.start();
            }
        });

        isStarted = true;
    }

    /**
     * Stop all running channel threads in this ChannelGroup
     */
    public synchronized void stop() {

        LOG.info("Stopping " + this.getClass().getName());

        if (!isStarted())
            return;

        // interrupt all
        threads.forEach((channel, thread) -> {
            LOG.info(String.format("Interrupting thread %s", thread.getName()));
            
            if (thread.isAlive()) {
                thread.interrupt();
            }
        });

        // join all interrupted
        threads.forEach((channel, thread) -> {
            LOG.info(String.format("Waiting for thread %s to join", thread.getName()));
            
            try {
                if (thread.isAlive()) {
                    thread.join(1000 * 10); // wait 10 seconds
                }
            } catch (InterruptedException e) {
                // ignore
            }
        });
    }

    public synchronized boolean isStarted() {
        return isStarted;
    }
}
