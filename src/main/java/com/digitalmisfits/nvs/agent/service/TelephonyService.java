package com.digitalmisfits.nvs.agent.service;


import com.digitalmisfits.nvs.agent.telephony.Channel;
import com.digitalmisfits.nvs.agent.telephony.ChannelOperator;
import com.digitalmisfits.nvs.agent.telephony.Subscriber;

import java.util.Collection;

public interface TelephonyService {

    public Collection<Subscriber> load(ChannelOperator channelOperator, int results);

    public void store(Subscriber subscriber, Channel.ConnectionState state);
}
