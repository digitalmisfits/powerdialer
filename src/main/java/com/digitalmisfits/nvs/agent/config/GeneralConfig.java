package com.digitalmisfits.nvs.agent.config;

import com.digitalmisfits.nvs.agent.domain.dto.NetworkOperatorDTO;
import com.digitalmisfits.nvs.agent.domain.entity.NetworkOperator;
import com.digitalmisfits.nvs.agent.telephony.ChannelOperator;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.EnableRetry;

@Configuration
@EnableRetry(proxyTargetClass = false)
public class GeneralConfig {

    @Bean
    public MapperFactory mapperFactory() {
        MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();

        mapperFactory.classMap(NetworkOperator.class, NetworkOperatorDTO.class)
                .field("destinationCodes", "networkDestinationCodes")
                .byDefault()
                .register();

        mapperFactory.classMap(NetworkOperator.class, ChannelOperator.class)
                .byDefault()
                .register();

        return mapperFactory;
    }
    
    @Bean
    public MapperFacade mapperFacade(MapperFactory mapperFactory) {
        return mapperFactory.getMapperFacade();
    }
}
