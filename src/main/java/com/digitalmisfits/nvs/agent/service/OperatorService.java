package com.digitalmisfits.nvs.agent.service;


import com.digitalmisfits.nvs.agent.telephony.ChannelOperator;
import com.digitalmisfits.nvs.agent.telephony.Subscriber;

import java.util.Collection;

public interface OperatorService {

    public Collection<ChannelOperator> queryChannelOperators();

    public ChannelOperator getChannelOperator(String country, String name);

    public Collection<Subscriber> getNumbers(ChannelOperator operator);
}
