package com.digitalmisfits.nvs.agent.domain.repository;


import com.digitalmisfits.nvs.agent.domain.entity.NetworkOperator;
import com.digitalmisfits.spring.aop.annotation.ResourceNotFound;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@ResourceNotFound
@Transactional(readOnly = true)
public interface NetworkOperatorRepository extends JpaRepository<NetworkOperator, Long>, JpaSpecificationExecutor<NetworkOperator> {

    @Query("SELECT n FROM NetworkOperator n WHERE LOWER(n.iso3166CountryCode) = ?1 AND LOWER(n.networkName) = ?2")
    NetworkOperator findGeographicalOperator(String country, String name);

    @Query("SELECT terminator FROM Dialplan dp JOIN dp.dialplanConnectors dpc JOIN dpc.terminationNetworkOperator terminator WHERE dpc.originationNetworkOperator.id = ?1 and dp.weight > 0 ORDER BY -(LOG(RAND()) / dp.weight)")
    List<NetworkOperator> findWeightOrderedOperators(long networkOperatorId, Pageable pageable);
}
