package com.digitalmisfits.nvs.agent.domain.entity;

import com.google.common.math.LongMath;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.math.RoundingMode;

/**
 * This entity is separated from the NetworkDestinationCode to prevent unnecessary locking on the destination code table
 */
@Entity
@Table(name = "ranges")
public class NetworkDestinationRange {

    public static enum RangeState {
        VALID,
        EXHAUSTED
    }

    @Id
    @GeneratedValue
    private Long id;

    /**
     * The normalized start of the range
     */
    @Min(1)
    @Column(nullable = false)
    private int start;

    /**
     * The normalized end of the range
     */
    @Min(1)
    @Column(nullable = false)
    private int end;

    /**
     * The expanded number of digits making up the destination range (e.a. +256 (0) 710 XXX XXX).
     * *
     * This is an important value. This value is used to construct correct (left aligned) padding for number generation
     */
    @Column(nullable = false)
    private int length;

    /**
     * The starting state for the  Linear Feedback Shit Register
     */
    @Min(1)
    @Column(nullable = false)
    private int seed;

    /**
     * The register's size. May be any value within  0 < x < 32
     */
    @Column(nullable = false)
    private int registerSize = 0;

    /**
     * Total number of transitions applied to the Linear Feedback Shit Register
     */
    @Column(nullable = false)
    private int transitions = 0;

    /**
     * Range state
     */
    @Column(nullable = false)
    //@Enumerated(EnumType.STRING)
    private RangeState state = RangeState.VALID;

    @OneToOne(mappedBy = "range", fetch = FetchType.LAZY)
    private NetworkDestinationCode destinationCode;

    /**
     * Calculate the (ceiling rounded) bit register size based on the end (max) value
     */
    @PrePersist
    public void calculateRegisterSize() {
        setRegisterSize(LongMath.log2(getEnd(), RoundingMode.CEILING));
    }

    public Long getId() {
        return id;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;

        // calculate the number of digits
        setLength((int) (Math.log10(start) + 1));
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getSeed() {
        return seed;
    }

    public void setSeed(int seed) {
        this.seed = seed;

        // increment number of transitions
        setTransitions(transitions + 1);

        // mark range as EXHAUSTED if the seed equals the original starting point
        if (seed == start)
            setState(RangeState.EXHAUSTED);
    }

    public int getRegisterSize() {
        return registerSize;
    }

    protected void setRegisterSize(int registerSize) {
        this.registerSize = registerSize;
    }

    public int getTransitions() {
        return transitions;
    }

    protected void setTransitions(int transitions) {
        this.transitions = transitions;
    }

    public RangeState getState() {
        return state;
    }

    protected void setState(RangeState state) {
        this.state = state;
    }

    public NetworkDestinationCode getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(NetworkDestinationCode destinationCode) {
        this.destinationCode = destinationCode;
    }
}
