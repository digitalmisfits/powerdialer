package com.digitalmisfits.nvs.agent.api.retrofit;

import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.DataRetrievalFailureException;
import retrofit.ErrorHandler;
import retrofit.RetrofitError;


public class RetrofitExceptionTranslator implements ErrorHandler {

    public static final ErrorHandler DEFAULT = new ErrorHandler() {
        @Override
        public Throwable handleError(RetrofitError cause) {
            return cause;
        }
    };

    @Override
    public Throwable handleError(RetrofitError cause) {

        switch (cause.getKind()) {
            case NETWORK:
                return new DataAccessResourceFailureException("Network not available");
            case HTTP: {
                return new DataRetrievalFailureException("Invalid response", cause);
            }
        }

        return cause;
    }
}
