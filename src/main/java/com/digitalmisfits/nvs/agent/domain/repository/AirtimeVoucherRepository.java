package com.digitalmisfits.nvs.agent.domain.repository;


import com.digitalmisfits.nvs.agent.domain.entity.AirtimeVoucher;
import com.digitalmisfits.nvs.agent.domain.entity.NetworkOperator;
import com.digitalmisfits.spring.aop.annotation.ResourceNotFound;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.LockModeType;
import java.util.List;

@Repository
@ResourceNotFound
@Transactional(readOnly = true)
public interface AirtimeVoucherRepository extends JpaRepository<AirtimeVoucher, Long>, JpaSpecificationExecutor<AirtimeVoucher> {

    @Transactional(readOnly = false)
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("SELECT v FROM AirtimeVoucher v WHERE v.networkOperator = ?1 and v.isAvailable = true ORDER BY RAND()")
    public List<AirtimeVoucher> findAvailableAirtimeVouchers(NetworkOperator operator, Pageable pageable);
}
